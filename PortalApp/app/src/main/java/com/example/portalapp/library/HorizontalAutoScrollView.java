package com.example.portalapp.library;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
//import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

/*
 * 
 * © Copyright 2013. 理音伊織
 * 参考先URL：http://relog.xii.jp/mt5r/2010/12/horizontalscrollview-part2.html
 * 
 * もなかぷろ (id:monakapro)
 * 参考先URL：http://monakap.hatenablog.com/entry/2013/08/02/130246
 * 
 */
public class HorizontalAutoScrollView extends HorizontalScrollView {
	
	private final String TAG = "HorizontalAutoScrollView";

	private final static String STR_ATTR_FRAME_INTERVAL = "frameInterval";	//フレーム間の時間
	private final static String STR_ATTR_FRAME_DELTA = "frameDelta";		//フレーム間の移動量
	private final static String STR_ATTR_LAPEL_INTERVAL = "lapelInterval";	//折り返す時のフレーム間隔
	private final static String STR_ATTR_IS_LOOP = "isLoop";				//ループするか
	private final static String STR_ATTR_LOOP_INTERVAL = "loopInterval";	//ループするときのフレーム間隔
	
	private Handler _handlerAnimation = null;		//アニメーション用
	private int _frameInterval = 20;				//フレーム間の時間
	private int _frameDelta = 1;					//フレーム間の移動量
	private int _lapelInterval = 200;				//折り返す時のフレーム間隔
	private boolean _isDerectionLeft = true;		//左へ動いているか
	private int _prev_x = 0;						//前回の場所
	
	private boolean _isLoop = false;				//ループするか
	private int _loopInterval = 200;				//ループする時のフレーム間隔
	
	private String _innerTextBackup = "";			//元々の文字列のバックアップ
	private int _innerTextWidth = 0;				//現在の文字列の長さ（2個分）

	//__________________________________________________________________________________
	private long _DEF_TEXT_SIZE = 0;				//テキストの長さ
	float _child_text_width = 0;					//子供のテキストビューの長さ（更新毎に変動する）
	private boolean _isInit = true;					//起動時のThreatか
	private boolean _isRunningThread = false;		//Threadが実行中か
	
	//__________________________________________________________________________________
	
	
	/**
	 * 描画スレッド
	 */
	private Runnable _runAnimationThread = new Runnable(){
		@Override
		public void run(){
			updateAutoScroll();
			
		}
	};

	/**
	 * アニメーション用のハンドラ
	 * @return
	 */
	private Handler getHandlerAnimation(){
		if(_handlerAnimation == null){
			_handlerAnimation = new Handler();
		}
		return _handlerAnimation;
	}

	/**
	 * 子供のテキストビューを取得する
	 * @return
	 */
	private TextView getInnerTextView(){
		TextView textView = null;
		try{
			View view = getChildAt(0);
			if(view.getClass() == TextView.class){
				textView = (TextView) view;
			}
		}catch(Exception e){
		}
		return textView;
	}
	
	/**
	 * テキストの長さが変化したか
	 * @return
	 */
	private boolean isChangeTextSize(){
		boolean flg = false;
		long text_len = getInnerTextView().getText().length();
		if(_DEF_TEXT_SIZE != text_len){
			_DEF_TEXT_SIZE = text_len;
			flg = true;
		}else{
			flg = false;
		}
		return flg;
	}
	
	/**
	 * ループでかつテキストが含まれるか
	 * @return
	 */
	private boolean isLoopMode(){
		return _isLoop && !isNullInnerTextView();
		
	}
	
	/**
	 * テキストがNullであるか
	 * @return
	 */
	private boolean isNullInnerTextView(){
		return getInnerTextView() == null;
		
	}
	
	/**
	 * 横スクロールするテキストビューのコンストラクタ
	 * @param context
	 * @param attrs
	 */
	public HorizontalAutoScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setSmoothScrollingEnabled(true);
		
		String temp = null;
		
		//フレーム間の時間
		temp = attrs.getAttributeValue(null, STR_ATTR_FRAME_INTERVAL);
		if(temp != null){
			_frameInterval = Integer.valueOf(temp);
		}
		//フレーム間の移動量
		temp = attrs.getAttributeValue(null, STR_ATTR_FRAME_DELTA);
		if(temp != null){
			_frameDelta = Integer.valueOf(temp);
		}
		//折り返し時のフレーム間隔
		temp = attrs.getAttributeValue(null, STR_ATTR_LAPEL_INTERVAL);
		if(temp != null){
			_lapelInterval = Integer.valueOf(temp);
		}
		
		//ループ時のフレーム間隔
		temp = attrs.getAttributeValue(null, STR_ATTR_LOOP_INTERVAL);
		if(temp != null){
			_loopInterval = Integer.valueOf(temp);
		}
		//ループするか
		temp = attrs.getAttributeValue(null, STR_ATTR_IS_LOOP);
		if(temp != null){
			_isLoop = Boolean.valueOf(temp);
		}
	}
	
	/**
	 * 表示状態が変わった
	 */
	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		super.onWindowVisibilityChanged(visibility);
		
		if(visibility == View.VISIBLE){
			startAutoScroll();
		}else{
			stopAutoScroll();
		}
	}

	/**
	 * 手動でスクロールできないようにする
	 */
	@SuppressLint("ClickableViewAccessibility") @Override
	public boolean onTouchEvent(MotionEvent ev) {
		return true;//super.onTouchEvent(ev);
		
	}
	
	/**
	 * 自動スクロールを開始する
	 */
	public void startAutoScroll(){
		
		if(!_isRunningThread){
			_isInit = true;
			//監視を開始
			getHandlerAnimation().postDelayed(_runAnimationThread, _loopInterval);
			
			_isRunningThread = true;
			
		}
		
	}
	
	/**
	 * 自動スクロールを止める
	 */
	public void stopAutoScroll(){
		
		if(_isRunningThread){
			//停止する
			getHandlerAnimation().removeCallbacks(_runAnimationThread);
			//位置を戻す
			scrollTo(0, getScrollY());
			
			_isRunningThread = false;
			
		}

	}
	
	/**
	 * 1つ目のテキストへ設定する
	 * @param text
	 */
	public void setText(String text){
		TextView tv = getInnerTextView();
		
		if(tv == null){
			//テキストがない
		}else{
			tv.setText(text);
			
			//Log.d(TAG, "Called setText-Module");
			
			//再起動
			this.stopAutoScroll();
			this.startAutoScroll();
			
			
		}
		
	}
	
	/**
	 * 1つ目のテキストを取得する
	 * @return
	 */
	public String getText(){
		return _innerTextBackup;
	}

	/**
	 * 自動スクロールの状態更新
	 */
	public void updateAutoScroll(){
		long next_interval = _frameInterval;
		int x = getScrollX();
		Paint paint = new Paint();
		
		//Log.d(TAG+"#Thread", "Thread Running");
		
		if(_isInit){
			//Log.d(TAG+"#isInit", "isInit");

			paint.setTextSize(getInnerTextView().getTextSize());
			_child_text_width = paint.measureText(getInnerTextView().getText().toString());
			
			_isInit = false;
		}
		
		if(isChangeTextSize()){
			//Log.d(TAG, "Changed");
			
		}
		
		if(getChildAt(0) == null){
		}else if(_child_text_width <= getWidth()){
			//はみ出てない
			next_interval *= 1024;	//スクロールの必要が無いので間隔を広げる
			
			_isDerectionLeft = true;
			_prev_x = 0;
			x = 0;
			
			_innerTextBackup = "";
			_innerTextWidth = 0;
		}else{
			//はみ出てる
			
			//Log.d(TAG+"#over", "text size over");
			
			if(isLoopMode()){
				//ループでかつ中にテキストが含まれる

				//内容を2重にする
				if(_innerTextWidth != 0){
					//既に内容を2重にしてある
				}else{
					//初めて
					_innerTextBackup = (String) getInnerTextView().getText();
					_innerTextWidth = (int) _child_text_width;
					getInnerTextView().setText(_innerTextBackup + _innerTextBackup);
					
					//Log.d(TAG+"#text", "width #"+_innerTextWidth+"text #"+_innerTextBackup);
					
				}

				//左へ
				x += _frameDelta;

				//予定の幅動いたら戻す
				if(_innerTextWidth <= x){
					x = 0;
					next_interval = _loopInterval;
				}
			}else{
				//左右移動

				//位置を計算
				if(_isDerectionLeft){
					//左へ
					x += _frameDelta;
				}else{
					//右へ
					x -= _frameDelta;
				}

				//向きを変える
				if(x == _prev_x){
					_isDerectionLeft = !_isDerectionLeft;
					next_interval = _lapelInterval;
					
				}
			}

			//Log.d(TAG+"#point", "prev_x #"+_prev_x+", now_x#"+x);
			
			_prev_x = x;
			
		}

		//移動
		scrollTo(x, getScrollY());
		
		//次のセット
		getHandlerAnimation().postDelayed(_runAnimationThread, next_interval);
	}
	
	
}

