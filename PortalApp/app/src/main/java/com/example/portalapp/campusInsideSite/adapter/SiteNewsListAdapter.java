package com.example.portalapp.campusInsideSite.adapter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.listInfo.HeaderInfo;


// 外から使用したかったためprivate を public に一時的に変更しました tomoya
public class SiteNewsListAdapter extends ArrayAdapter<HeaderInfo> {
	LayoutInflater inflater;

	public SiteNewsListAdapter(Context context, List<HeaderInfo> objects) {
		super(context, -1, objects);
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item, parent, false);
		}

		HeaderInfo head = getItem(position);
		TextView textView_title = (TextView) convertView.findViewById(R.id.textView_title);
		TextView textView_org = (TextView) convertView.findViewById(R.id.textView_org);
		TextView textView_date = (TextView) convertView.findViewById(R.id.textView_date);
		ImageView imageView1 = (ImageView) convertView.findViewById(R.id.imageView_ico);
		
		textView_title.setText(head.title);
		textView_org.setText(head.author);
		textView_date.setText(head.date);
		textView_org.setTextColor(Color.rgb(120, 120, 120));
		textView_date.setTextColor(Color.rgb(120, 120, 210));
		
		if (isInclude("[重要]", head.title)) {
			imageView1.setImageResource(R.drawable.important);
		} else if (isInclude("[緊急]", head.title)) {
			imageView1.setImageResource(R.drawable.emergency);
		} else {
			if(matchImgType(head.type,"message")){
				imageView1.setImageResource(R.drawable.message);
			}else{
				imageView1.setImageResource(R.drawable.graduation);
			}
		}
		
		return convertView;
	}
	
	/**
	 * 
	 * @param hash
	 * @param cmp
	 * @return
	 */
	private boolean matchImgType(String hash, String cmp){
		boolean flg = false;
		if(hash.indexOf(cmp) != -1) flg = true;
		return flg;
	}
	
	/**
	 * 
	 * @param regexp
	 * @param text
	 * @return
	 */
	private boolean isInclude(String regexp, String text){
		Pattern p = Pattern.compile(".*("+regexp+"){1}.*");
		Matcher m = p.matcher(text);
		
		return m.find();
	}
	
}
