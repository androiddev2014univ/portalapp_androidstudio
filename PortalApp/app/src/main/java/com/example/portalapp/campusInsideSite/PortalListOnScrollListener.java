package com.example.portalapp.campusInsideSite;

import com.example.portalapp.R;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class PortalListOnScrollListener implements OnScrollListener{
	private int _pos = 0;
	private boolean toggle_down = true, _isBottom = false;
	
	private ListView myselfListView;
	private ImageButton imgBtn;
	private FrameLayout frame_reload_btn;
	private Activity activity;
	
	public PortalListOnScrollListener(ListView mListView, Activity activity){
		this.myselfListView = mListView;
		this.activity = activity;
		this.imgBtn = (ImageButton) this.activity.findViewById(R.id.btnReloadPortal);
		this.frame_reload_btn = (FrameLayout) this.activity.findViewById(R.id.frame_reload_portalList);
		
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO 自動生成されたメソッド・スタブ
	    
	    if(isDown(_pos)){
	    	if(toggle_down){
	    		// 下にスクロールされはじめた時で、更新ボタンが活性化されている時
	    		ObjectAnimator.ofFloat(frame_reload_btn, "translationY", 300).start();
	    		imgBtn.setEnabled(false);
	    		
	    	}
	    	toggle_down = false;
	    }else if(isUp(_pos)){
	    	if(!toggle_down){
	    		// 上にスクロールされはじめたとき
	    		ObjectAnimator.ofFloat(frame_reload_btn, "translationY", 0).start();
	    		imgBtn.setEnabled(true);
	    		
	    	}
			
	    	toggle_down = true;
	    }
	    
	    if(totalItemCount == firstVisibleItem + visibleItemCount){
	    	_isBottom = true;
	    	
	    }else{
	    	_isBottom = false;
	    	
	    }
		
	}
	
	/**
	 * 下にスクロールされているか
	 * @param position
	 * @return
	 */
	private boolean isDown(int position){
		return checkPositionToggle(position, 0, false);
		
	}
	/**
	 * 上にスクロールされているか
	 * @param position
	 * @return
	 */
	private boolean isUp(int position){
		return checkPositionToggle(position, 1, true);
		
	}
	/**
	 * 
	 * @param pos：関数が呼び出される前のリストのポジション
	 * @param interval：判定を無視するリストの移動量
	 * @param isModeUp：判定モード（上にスクロールされる => true）
	 * @return
	 */
	private boolean checkPositionToggle(int pos, int interval, boolean isModeUp){
		try{
			int pos_now = myselfListView.getFirstVisiblePosition();
			final boolean flg = (isModeUp)? pos - pos_now > interval : pos_now - pos > interval;
			if(flg){
				_pos = pos_now;
				return true;
			}
			return false;
		}catch(Exception e){
			return false;
		}
		
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO 自動生成されたメソッド・スタブ
		if(_isBottom && scrollState == OnScrollListener.SCROLL_STATE_IDLE){
			onBottom(view, scrollState);
			
		}
		
	}
	
	public void onBottom(AbsListView view, int scrollState){
		//Toast.makeText(activity, "OK! This ListView is Bottom, now.", Toast.LENGTH_SHORT).show();
		
	}

}
