package com.example.portalapp.campusInsideSite.extJsoup;

import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;

import com.example.portalapp.campusInsideSite.listInfo.HtmlHash;

public class JsoupParse {
	private HtmlHash[] hash = new HtmlHash[10];
	private Document doc;
	
	public void setDoc(Document doc) {
		this.doc = doc;
		
	}
	public void doParse() {
		final Elements elements = this.doc.getElementsByTag("a");
		final Element element = this.doc.getElementsByTag("form").first();
		final String[] parts_br = element.html().split("<br />");
		final String[] parts_hr = element.html().split("<hr />");
		for(int i=0;i<10;i++) {
			String title;
			if(elements.get(i).html().indexOf("<b>") != -1){
				String[] parts_b = elements.get(i).html().split("<b>");
				parts_b = parts_b[1].split("</b>");
				title = parts_b[0];
			}else{
				title = elements.get(i).ownText();
			}
			String date = parts_br[5*i+4].replaceAll("受信日時", "Date");
			String org = parts_br[5*i+5].replaceAll("送信元", "From");
			//org += "\t";
			String type;
			if(i == 0) {
				String[] tmp = parts_hr[2].split("<br />");
				type = tmp[0];
			}else{
				type = parts_br[5*i+2];
			}
			if(Pattern.compile("お知らせ").matcher(type).find()) {
				type = "univ";
			} else {
				type = "message";
			}
	        String linkHref = elements.get(i).attr("href");
			hash[i] = new HtmlHash(
					title.replaceAll("\n", ""),
					date.replaceAll("\n", ""),
					org.replaceAll("\n", ""),
					type.replaceAll("\n", ""),
					linkHref.replaceAll("\n", "")
					);
		}
	}
	public HtmlHash[] getHash() {
		return hash;
	}
	
}
