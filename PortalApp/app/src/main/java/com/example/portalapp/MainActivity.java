package com.example.portalapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import android.support.v7.app.ActionBarActivity;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.portalapp.about.AboutActivity;
import com.example.portalapp.bustime.*;
import com.example.portalapp.campusInsideSite.PortalListActivity;
import com.example.portalapp.lawRelation.LawRelationActivity;
import com.example.portalapp.library.HorizontalAutoScrollView;
import com.example.portalapp.library.InitialRunning;
import com.example.portalapp.license.LicenseActivity;
import com.example.portalapp.oitNewsDetail.OitNewsDetailActivity;
import com.example.portalapp.settings.*;

public class MainActivity extends ActionBarActivity
        implements OnItemClickListener, OnItemLongClickListener, OnClickListener{

	private OitNewsLatest newsLists, reloadNewsLists;
	private ProgressDialog pgdialog;
	private ImageButton btn_reload;
	private ListView mListView;
	private HorizontalAutoScrollView viewScroll;
    private final Handler handler = new Handler();
    private FrameLayout frame_reload_btn;
    private ObjectAnimator rotate;
    private LoadBusTime loadBusTime;

    
    /**
     * テキストをセットする
     */
    private void setBusText(){
    	final Handler handler = new Handler();
    	new Thread(){
    		@Override
    		public void run(){
    			try{
    				LoadBusInfoThread th = new LoadBusInfoThread(handler);
    				th.start();
    				th.join();
    				
        			handler.post(new Runnable() {
    					@Override
    					public void run() {
    						// TODO 自動生成されたメソッド・スタブ
    						if(viewScroll == null){
    							viewScroll = (HorizontalAutoScrollView) findViewById(R.id.AutoScroll_BusTime);
    						}
    						
    						try{
        						viewScroll.setText(makeBusText());
    							
    						}catch(BusEndException e){
    							setBusText("本日は終了しました");
    							
    						}catch(Exception e){
    							setBusText("エラー："+e.toString());
    							
    						}
    						
    					}
    				});
    				
    			}catch(final Exception e){
    				handler.post(new Runnable() {
						@Override
						public void run() {
							// TODO 自動生成されたメソッド・スタブ
							//setBusText("エラー：読み込みに失敗しました");
							setBusText("エラー："+e.toString());
				    		
						}
					});
    				
    			}
    		}
    	}.start();
    	
    }
    
    private class LoadBusInfoThread extends Thread{
    	private Handler handler;
    	
    	public LoadBusInfoThread(Handler handler){
    		this.handler = handler;
    	}
    	
    	@Override
    	public void run(){
	        if(loadBusTime == null){
	        	try{
		        	loadBusTime = new LoadBusTime(
		        			getString(R.string.keihanbus_script_url),
		        			getResources().getStringArray(R.array.query_lists));

		        	handler.post(new Runnable() {
						@Override
						public void run() {
							// TODO 自動生成されたメソッド・スタブ
							//Toast.makeText(MainActivity.this, "バスデータ読み込み完了", 0).show();
						}
					});
		        	
	        	}catch(Exception e){
	        		setBusText("エラー："+e.toString());
	        		
	        	}
	        }
	        
    	}
    	
    }

    /**
     * テキストをセットする
     */
    private void setBusText(String str){
    	try{
        	viewScroll.setText(""+str);
    		
    	}catch(Exception e){
    		setBusText("エラー："+e.toString());
    		
    	}
    	
    }
    
    /**
     * バスデータが無いときの例外処理クラス
     */
    private class BusEndException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public BusEndException(){
    		super("Bus Time Table End Exception");
    	}
    }

    /*
     * 午前は行きのバス、午後は帰りのバスを表示
     */
    private String makeBusText() throws BusEndException {
        String text = "";

        TreeMap<String, String> nagaoBusTime;
        TreeMap<String, String> keihanBusTime;
        
        Calendar date = Calendar.getInstance();

        int nowHour = date.get(Calendar.HOUR_OF_DAY);
        int nowMinute = date.get(Calendar.MINUTE);
        
        String time = String.format("%02d:%02d", nowHour, nowMinute);
        
        if(nowHour < 11) {
            nagaoBusTime = loadBusTime.getNagaoToUnivBusTime();
            keihanBusTime = loadBusTime.getKeihanToUnivBusTime();
        }
        else {
            nagaoBusTime = loadBusTime.getUnivToNagaoBusTime();
            keihanBusTime = loadBusTime.getUnivToKeihanBusTime();
        }
        
        Map.Entry<String, String> nagao = nagaoBusTime.higherEntry(time);
        Map.Entry<String, String> keihan = keihanBusTime.higherEntry(time);

        text = "";
        if(nagao != null){
            text += "[JR線]" + nagao.getKey() + "発 " + nagao.getValue() + "行き  ";
        	
        }
        if(keihan != null){
            text += "[京阪線]" + keihan.getKey() + "発 " + keihan.getValue() + "行き ";
        	
        }
        
        if(nagao == null && keihan == null){
        	throw new BusEndException();
        }
        
        return text;
    }
    
    // 新着情報を取得する
    private void reloadNewList(){
        new Thread(){
        	@Override
        	public void run(){
    			
	        	try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO 自動生成された catch ブロック
					e1.printStackTrace();
				}
	        	
		        try {
		        	reloadNewsLists = new OitNewsLatest();
		            final ArrayList<ListInfo> info = new ArrayList<ListInfo>();
		            reloadNewsLists.init();
			        for(int i=0;i<reloadNewsLists.get_list_num();i++){
			        	ListInfo list = new ListInfo();
			        	list.msg = reloadNewsLists.get_msg(i);
			        	list.date = reloadNewsLists.get_date(i);
			        	list.title = reloadNewsLists.get_title(i);
			        	list.kategorie = reloadNewsLists.get_kategorie(i);
			        	list.link = reloadNewsLists.get_link(i);
			        	info.add(list);
			        	
			        }
			        handler.post(new Runnable(){
			        	public void run(){

			    			// ListViewのポジションを取得
			    			int position = 0, distance = 0;
			    			try {
			    		        position = mListView.getFirstVisiblePosition();
			    		        distance = mListView.getChildAt(0).getTop();
			    			} catch (Exception e){
			    				e.printStackTrace();
			    			}
			    			
			                mListView.setAdapter(new NewsAdapter(MainActivity.this, info));
			                mListView.setSelectionFromTop(position, distance);
			                
				        	newsLists = reloadNewsLists;
				            
			        	}
			        });
				} catch (Exception e) {
					// TODO 自動生成された catch ブロック
					Log.e("Jsoup-Exception", e.toString());
			        handler.post(new Runnable(){
			        	public void run(){
			        		Toast.makeText(MainActivity.this, "更新に失敗しました", Toast.LENGTH_SHORT).show();
			        	
			        	}
			        });
				}
		        
		        try{
		        	handler.post(new Runnable(){
		        		@Override
		        		public void run(){
					        // 回転アニメーションを終了
				        	rotate.cancel();
				        	rotate.end();
				        	rotate = null;
			                // 更新ボタンをアクティブにする
				        	btn_reload.setEnabled(true);
		        			
		        		}
		        	});
		        	
		        }catch(Exception e){
		        	Log.e("FloatButton-End-Exception", e.toString());
		        	
		        }

        	}
        }.start();
        
    }
    
    @SuppressLint("HandlerLeak") private Handler handlerPlog = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	// ロード終了
        	pgdialog.dismiss();
        }
    };
    
    // ___________________________________________________________________________________________________________________

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        pgdialog = new ProgressDialog(this);
        pgdialog.setMessage("データ取得中...");
        pgdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pgdialog.setCancelable(false);
        
        pgdialog.show();

        mListView = (ListView) findViewById(R.id.latest_list);
        btn_reload = (ImageButton) findViewById(R.id.btnReload);
        btn_reload.setOnClickListener(this);
        frame_reload_btn = (FrameLayout) findViewById(R.id.frame_reload_btn);
        viewScroll = (HorizontalAutoScrollView) findViewById(R.id.AutoScroll_BusTime);
        
        // 新着情報を取得する
        new Thread(){
        	@Override
        	public void run(){
		        try {
		            newsLists = new OitNewsLatest();
		            final ArrayList<ListInfo> info = new ArrayList<ListInfo>();
					newsLists.init();
			        for(int i=0;i<newsLists.get_list_num();i++){
			        	ListInfo list = new ListInfo();
			        	list.msg = newsLists.get_msg(i);
			        	list.date = newsLists.get_date(i);
			        	list.title = newsLists.get_title(i);
			        	list.kategorie = newsLists.get_kategorie(i);
			        	list.link = newsLists.get_link(i);
			        	info.add(list);
			        	
			        }
			        handler.post(new Runnable(){
			        	public void run(){
			                mListView.setAdapter(new NewsAdapter(MainActivity.this, info));
			        	}
			        });
				} catch (Exception e) {
					// TODO 自動生成された catch ブロック
					Log.e("Jsoup-Exception", e.toString());
				}
		        handlerPlog.sendEmptyMessage(0);
        	}
        	
        }.start();
        
        // クリックと長いクリックのリスナー登録
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        
        mListView.setOnScrollListener(new OnScrollListener(){
        	private int pos_tmp = 0;
        	private boolean toggle_down = true, _isBottom = false;

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO 自動生成されたメソッド・スタブ
		        if(isDown(pos_tmp)){
		        	if(toggle_down){
		        		// 下にスクロールされはじめた時で、更新ボタンが活性化されている時
		        		ObjectAnimator.ofFloat(frame_reload_btn, "translationY", 300).start();
		        		btn_reload.setEnabled(false);
		        		
		        	}
		        	toggle_down = false;
		        }else if(isUp(pos_tmp)){
		        	if(!toggle_down){
		        		// 上にスクロールされはじめたとき
		        		ObjectAnimator.ofFloat(frame_reload_btn, "translationY", 0).start();
		        		btn_reload.setEnabled(true);
		        		
		        	}
		        	toggle_down = true;
		        }

			    if(totalItemCount == firstVisibleItem + visibleItemCount){
			    	_isBottom = true;
			    	
			    }else{
			    	_isBottom = false;
			    	
			    }
			    
			}
			
			/**
			 * 下にスクロールされているか
			 * @param p
			 * @return
			 */
			private boolean isDown(int p){
				return checkPositionToggle(p, 0, false);
				
			}
			/**
			 * 上にスクロールされているか
			 * @param p
			 * @return
			 */
			private boolean isUp(int p){
				return checkPositionToggle(p, 0, true);
				
			}
			/**
			 * 
			 * @param pos：関数が呼び出される前のリストのポジション
			 * @param interval：判定を無視するリストの移動量
			 * @param isModeUp：判定モード（上にスクロールされる => true）
			 * @return
			 */
			private boolean checkPositionToggle(int pos, int interval, boolean isModeUp){
				try{
					int pos_now = mListView.getFirstVisiblePosition();
					final boolean flg = (isModeUp)? pos - pos_now > interval : pos_now - pos > interval;
					if(flg){
						pos_tmp = pos_now;
						return true;
					}
					return false;
				}catch(Exception e){
					return false;
				}
				
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO 自動生成されたメソッド・スタブ
				if(_isBottom && scrollState == OnScrollListener.SCROLL_STATE_IDLE){
					Toast.makeText(MainActivity.this, "記事はこれ以上ありません", Toast.LENGTH_SHORT).show();
					
				}
				
			}
			
        });
        
        setBusText("時刻表データを読み込み中...");
        
        InitialRunning initTask = new InitialRunning(this);
        initTask.InitialRun();
    }

	@Override
	public void onStart(){
		super.onStart();
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		setBusText();
		
	}
	
    @Override
    public void onStop(){
    	super.onStop();
    	setBusText("");
    	
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    	setBusText("");
    	
    }
    
    public void onClick(View v) {
		// TODO 自動生成された catch ブロック
    	if(v.getId() == R.id.btnReload){ // 更新ボタンが押された
    		/*
    		 * 
    		 * リストビューを更新
    		 * 
    		 * 
    		 */

    		try {
    			rotate = ObjectAnimator.ofFloat(btn_reload, "rotation", 0, 360 * 2);
    			rotate.setDuration(1000);
    			rotate.setRepeatCount(ObjectAnimator.INFINITE);
    			// 回転アニメーションを開始
				rotate.start();
    			// 更新ボタンを非アクティブにする
    			btn_reload.setEnabled(false);
    			
    		} catch(Exception e) {
	        	Log.e("FloatButton-Start-Exception", e.toString());
    			
    		}
    		
	        
			// ListViewの情報をリロード
			this.reloadNewList();

			
			/*
			 * 
			 * 自動スクロールのテキストビューを表示
			 * 
			 * 
			 */
			setBusText();

    	}
    	
    	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
		Intent intent = null;
		int id = item.getItemId();
		// 表示させる --------------------------------------------------------------------------
		// 表示非表示は、res/menu/main.xmlを参照
		if(id == R.id.action_portal_insite){
            intent = new Intent(this, PortalListActivity.class);
		}else if(id == R.id.action_bus_time){
			intent = new Intent(this, BusTimeActivity.class);
		// 表示させない ------------------------------------------------------------------------
		}else if (id == R.id.action_settings) {
		    intent = new Intent(this, PreferenceActivity.class);
		}else if(id == R.id.action_license){
		    intent = new Intent(this, LicenseActivity.class);
		}else if(id == R.id.action_law_relation){
			intent = new Intent(this, LawRelationActivity.class);
		}else if(id == R.id.action_about_app){
			intent = new Intent(this, AboutActivity.class);
		//-----------------------------------------------------------------------------------
		}else{
			return super.onOptionsItemSelected(item);
		}
	    this.startActivity(intent);
		return true;
    }
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO 自動生成されたメソッド・スタブ
		Intent intent;
		if(!newsLists.get_link(position).endsWith(".pdf")){
			// PDFをダウンロード
			intent = new Intent(this, OitNewsDetailActivity.class);
			intent.putExtra("url", newsLists.get_link(position));
		}else{
			// PDFをGoogleのビューサイトで表示
			String url = newsLists.get_link(position);
			intent = new Intent(Intent.ACTION_VIEW);
			// GoogleのビューサイトでPDFを表示
			intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + url), "text/html");
		}
	    this.startActivity(intent);
	}
	
	int onItemLongClick_Position;
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		onItemLongClick_Position = position;
		if(!newsLists.get_link(position).endsWith(".pdf")){
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setMessage("外部ブラウザに接続しますか？");
			alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO 自動生成されたメソッド・スタブ
					Intent intent;
					String url = newsLists.get_link(onItemLongClick_Position);
					intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.parse(url), "text/html");
					startActivity(intent);
				}
			});
			alert.setNegativeButton("Cancel", null);
			alert.create().show();
			
		}else{
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setMessage("外部ブラウザに接続して、pdfファイルをダウンロードしますか？");
			alert.setPositiveButton("OK" , new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO 自動生成されたメソッド・スタブ
					Intent intent;
					String url = newsLists.get_link(onItemLongClick_Position);
					intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.parse(url), "text/html");
					startActivity(intent);
				}
			});
			alert.setNegativeButton("Cancel", null);
			alert.create().show();
			
		}
		return true;
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
    	if (keyCode == KeyEvent.KEYCODE_BACK){
    		new AlertDialog.Builder(this)
    		.setTitle("アプリケーションの終了")
    		.setMessage("アプリケーションを終了しますか？")
    		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				// TODO 自動生成されたメソッド・スタブ
    				moveTaskToBack(true);
    				//MainActivity.this.finish();
    			}
    		})
    		.setNegativeButton("Cancel", null)
    		.show();
    		
    		return true;
    	}
    	return false;
    }
}
