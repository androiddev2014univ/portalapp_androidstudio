package com.example.portalapp.campusInsideSite;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import org.jsoup.nodes.Document;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.adapter.PortalListFragmentPagerAdapter;
import com.example.portalapp.campusInsideSite.adapter.SiteNewsListAdapter;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupLogin;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupParse;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupParse_Detail;
import com.example.portalapp.campusInsideSite.fragment.ConfigAlertDialogFragment;
import com.example.portalapp.campusInsideSite.fragment.MessagePortalListFragment;
import com.example.portalapp.campusInsideSite.fragment.UnivPortalListFragment;
import com.example.portalapp.campusInsideSite.listInfo.HeaderInfo;
import com.example.portalapp.campusInsideSite.listInfo.HtmlHash;
import com.example.portalapp.campusInsideSite.listInfo.HtmlHash_Detail;
import com.example.portalapp.campusInsideSite.listInfo.ListLocation;
import com.example.portalapp.library.SP;
import com.example.portalapp.library.sqlite.ReadDB;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class PortalListActivity extends ActionBarActivity implements OnItemClickListener, OnClickListener {
	private final Context context = this;
	private final Activity activity = this;
	private String SharedPreferenceFileName, SharedPreferenceFileName_LoadPage;
	private String LatestNewsTimeValue, LoadPageNumTagKey;
    private Handler handler;
    private ProgressDialog progressDialog;
    private boolean connection_er_flg = false;
    private boolean _isFullAdded = false;
    private boolean _isRunningListViewThread = false;
    private UnivPortalListFragment univFragment;
    private MessagePortalListFragment messageFragment;
    private ReadDB loginInfo;
    // Cookieはこのアクテビティが破棄されるまで保持されます
    private Map<String, String> cookies;
	private ImageButton btn_reload;
	private ArrayList<HeaderInfo> univTitles, messageTitles;
    private LoadPage loadPage = new LoadPage();
    // メッセージ用ハンドラのインスタンスを生成
    @SuppressLint("HandlerLeak") private final Handler handlerPlog = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	// プロセスダイアログを解放する
        	progressDialog.dismiss();
        }
    };
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portal_list);
        
    	progressDialog = new ProgressDialog(this);
    	progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	progressDialog.setCancelable(false);
        
    	SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_notifi);
    	LatestNewsTimeValue = getString(R.string.shared_preferences_notifi_tag_name_LatestNewsTimeValue);
    	SharedPreferenceFileName_LoadPage = getString(R.string.shared_preferences_file_name_loadPage);
    	LoadPageNumTagKey = getString(R.string.shared_preferences_loadPage_tag_name_loadPageSignal);
    	
        btn_reload = (ImageButton) findViewById(R.id.btnReloadPortal);
        btn_reload.setOnClickListener(this);
        
		// それぞれのArrayListのインスタンスを生成
		univTitles = new ArrayList<HeaderInfo>();
		messageTitles = new ArrayList<HeaderInfo>();
        
        FragmentManager manager = getSupportFragmentManager();
        ViewPager viewPager = (ViewPager)findViewById(R.id.portal_list_pager);
        PagerTabStrip pagerTabStrip = (PagerTabStrip)findViewById(R.id.portal_list_pager_tab_strip);
        PortalListFragmentPagerAdapter fragmentPagerAdapter = new PortalListFragmentPagerAdapter(manager, this);
        viewPager.setAdapter(fragmentPagerAdapter);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColorResource(R.color.white);
        univFragment = UnivPortalListFragment.getInstance();
        univFragment.setOnItemClickListener(this);
        messageFragment = MessagePortalListFragment.getInstance();
        messageFragment.setOnItemClickListener(this);
        
    	handler = new Handler();
    	loginInfo = new ReadDB(getApplicationContext());
    	
    	// リスト情報取得（別スレッド）
    	this.loadViewContents(true);
        
    	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
    }
    @Override
    public void onResume(){
    	super.onResume();
    	
    }
    @Override
    public void onStart(){
    	super.onStart();
    	
    }
	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		int id = v.getId();
		if(id == R.id.btnReloadPortal){ // 更新ボタンが押された
	        
			// ListViewの情報をリロード
			this.loadViewContents(false);
			
		}
		
	}
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	this.loadViewDetailContent(position);
        
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	case R.id.action_portal_list_config:
    		showDialog();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.portal_list, menu);
        return true;
    }
    @Override
    protected void onDestroy(){
    	super.onDestroy();
		if(connection_er_flg) {
			showToast("[ 通信エラー ]"
					+"\nユーザー情報・利用時間および通信環境を確認の上、もう一度接続してみてください。", 1);
			
		}
    	
    }
    
    
    
    
    // _______________________________________________________________________________________________________________________
    
    /**
     *  トーストを表示する
     * @param text
     * @param duration
     */
    private void showToast(String text, int duration){
    	Toast.makeText(context, text, duration).show();
    	
    }
    
    /**
     * 　設定ダイアログを表示する
     */
    private void showDialog(){
    	// Log.d("test", "ClickedDialog");
        ConfigAlertDialogFragment fragment = new ConfigAlertDialogFragment(this);
        fragment.show(getSupportFragmentManager(), "alert_dialog");
    	
    }
    
    /**
     *  リストの位置を取得
     * @return
     */
    private ListLocation getListPosition(){
    	final ListLocation location = new ListLocation();
    	
		// ListViewのポジションを取得
		location.clearLocation();
		try {
			location.univ_position = univFragment.mListView.getFirstVisiblePosition();
			location.univ_distance = univFragment.mListView.getChildAt(0).getTop();
			location.message_position = messageFragment.mListView.getFirstVisiblePosition();
			location.message_distance = messageFragment.mListView.getChildAt(0).getTop();
			
		} catch (Exception e){
			e.printStackTrace();
			
		}
		
		return location;
		
    }
    
    /**
     *  リストの位置をセットする
     * @param location
     */
    private void setListPosition(ListLocation location){
		univFragment.mListView.setSelectionFromTop(location.univ_position, location.univ_distance);
		messageFragment.mListView.setSelectionFromTop(location.message_position, location.message_distance);
    	
    }
    
    /**
     *  アダプタに情報をセットする
     */
    private void setAdapter(){
		univFragment.setAdapter(new SiteNewsListAdapter(context, univTitles));
		messageFragment.setAdapter(new SiteNewsListAdapter(context, messageTitles));
		
    }
    
    /**
     *  スクロールリスナーをセットする
     */
    private void setOnScrollListener(){
        ListView univListView, messageListView;
        
        univListView = (ListView) findViewById(R.id.univ_portal_list);
        messageListView = (ListView) findViewById(R.id.message_portal_list);
		univListView.setOnScrollListener(new PortalListOnScrollListener(univListView, activity){
			@Override
			public void onBottom(AbsListView view, int scrollState){
				addViewContents();
			}
		});
		messageListView.setOnScrollListener(new PortalListOnScrollListener(messageListView, activity){
			@Override
			public void onBottom(AbsListView view, int scrollState){
				addViewContents();
			}
		});
    	
    }

    /**
     * 
     * @param isInitMode：初期読み込みである場合はtrue
     */
    private void loadViewContents(final boolean isInitMode){
		// それぞれのArrayListのインスタンスを生成（初期化）
		univTitles = new ArrayList<HeaderInfo>();
		messageTitles = new ArrayList<HeaderInfo>();
		
    	if(isInitMode) {
    		// 初期読み込み処理 -------------------------------------------------------------
    		if(!_isRunningListViewThread){
    			_isRunningListViewThread = true;
            	LoadViewThread th = new LoadViewThread(true);
            	th.start();
    			
    		}
        	
    	}else{
    		// 再読み込み（更新）処理 --------------------------------------------------------
    		if(!_isRunningListViewThread){
    			_isRunningListViewThread = true;
    			
        		ReLoadViewThread th = new ReLoadViewThread();
        		th.start();
    		}
    		
    	}
    	
    	
    }

	private class LoadViewThread extends Thread {
		private boolean isShowDialog;
		
		public LoadViewThread(boolean isShowDialog){
			this.isShowDialog = isShowDialog;
			if(isShowDialog){
	    		// プロセスダイアログを表示
	        	progressDialog.setMessage("ログイン中...");
	    		progressDialog.show();
				
			}
		}
		
		@Override
		public void run() {
			try {
	    		
				HtmlHash[] hash = new HtmlHash[10];
				Document doc;
				HeaderInfo header;
	        	// Jsoupを用いたログイイン及びパース処理を行うオブジェクトのインスタンスを生成
				JsoupLogin obj = new JsoupLogin();
				JsoupParse parse = new JsoupParse();
				
				// ユーザー名とパスワードのDB検索、ログイン処理を実行する
				AccessUserInfoThread th = new AccessUserInfoThread(isShowDialog, obj, loginInfo);
				th.start();
				// 待機
				th.join();
				
				// SharedPreferencesから読込ページ数を取得
				SP sp = new SP(SharedPreferenceFileName_LoadPage, context);
				loadPage.end = sp.readInt(LoadPageNumTagKey);
				loadPage.start = 0;
				
				/*
				 * accessPageNum : アクセスするWebページ数(1ページに10記事ある)
				 * i : アクセスしたページの何番目の記事を処理するか
				 */
				for(int accessPageNum = loadPage.start; accessPageNum < loadPage.end +1; accessPageNum++){
					// 指定ページのニュース一覧のドキュメントを取得
					doc = obj.getPortalPage(accessPageNum);
					// ドキュメントをパースオブジェクトにセット
					parse.setDoc(doc);
					// パースの実行
					parse.doParse();
					// パース結果をHtmlHashオブジェクトとして返却
					hash = parse.getHash();

					for(int i=0;i<10;i++){
						// List1つ分の情報オブジェクトのインスタンスを取得
						header = new HeaderInfo(
								hash[i].title, hash[i].org, hash[i].date, hash[i].type,
								accessPageNum, i);
						
						// ArrayListに追加
						if(hash[i].type.equals("univ")) { univTitles.add(header); }
						else if(hash[i].type.equals("message")) { messageTitles.add(header); }
						
						// 最新日時の更新
						if(accessPageNum == 0 && i == 0) { setLatestNewsTime(hash); }
						
					}
				}
				// クッキー保管
				cookies = obj.getCookies();
				
				handler.post(new Runnable() {
					@Override
					public void run() {
						setAdapter();
						setOnScrollListener();

						// ログインユーザを表示する
						showToast("ログインユーザ："+loginInfo.getUsername(), 1);
						
						_isRunningListViewThread = false;
						
					}
				});
				
			}catch(Exception e) {
				e.printStackTrace();
				connection_er_flg = true;
				finish();
				
			}
			if(isShowDialog){
				// プロセスダイアログを非表示にする
				handlerPlog.sendEmptyMessage(0);
				
			}
	        
		}
			
	}
	
	private class AccessUserInfoThread extends Thread {
		private JsoupLogin obj;
		private ReadDB loginInfo;
		private boolean isShowDialog;
		
		public AccessUserInfoThread(boolean isShowDialog, JsoupLogin obj, ReadDB loginInfo){
			this.obj = obj;
			this.loginInfo = loginInfo;
			this.isShowDialog = isShowDialog;
			
		}
		
		@Override
		public void run() {
			// 名前とパスワードをDBから読み込み
        	loginInfo.init();
			// ユーザIDとパスワードをログインオブジェクトに挿入
			obj.setInfo(loginInfo.getUsername(), loginInfo.getPassword());
			
			try {
				// ログインを実行する（Cookieの取得＋初回接続）
				obj.login();
				
			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				handler.post(new Runnable(){
					@Override
					public void run() {
						// TODO 自動生成されたメソッド・スタブ
						showToast("ログインに失敗しました", 0);
						
					}
				});
				
			}
			
			if(isShowDialog){
				handler.post(new Runnable(){
					@Override
					public void run(){
			        	progressDialog.setMessage("データ取得中...");
			    		
					}
				});
				
			}
			
			
		}
		
	}
	
	private class ReLoadViewThread extends Thread {
		ObjectAnimator rotate;
		
		public ReLoadViewThread() {
			// TODO 自動生成されたコンストラクター・スタブ
    		try {
    			rotate = ObjectAnimator.ofFloat(btn_reload, "rotation", 0, 360 * 2);
    			rotate.setDuration(1000);
    			rotate.setRepeatCount(ObjectAnimator.INFINITE);
    			// 回転アニメーションを開始
				rotate.start();
    			// 更新ボタンを非アクティブにする
    			btn_reload.setEnabled(false);
    			
    		} catch(Exception e) {
	        	Log.e("FloatButton-Start-Exception", e.toString());
    			
    		}
    		
		}
		
		@Override
		public void run() {
    		
			try {
				HtmlHash[] hash = new HtmlHash[10];
				Document doc;
				HeaderInfo header;
	        	// Jsoupを用いたログイイン及びパース処理を行うオブジェクトのインスタンスを生成
				JsoupLogin obj = new JsoupLogin();
				JsoupParse parse = new JsoupParse();
				
	        	// クッキーを渡す
				obj.setCookies(cookies);
				
				loadPage.start = 0;
				
				/*
				 * accessPageNum : アクセスするWebページ数(1ページに10記事ある)
				 * i : アクセスしたページの何番目の記事を処理するか
				 */
				for(int accessPageNum = loadPage.start; accessPageNum < loadPage.end +1; accessPageNum++){
					// 指定ページのニュース一覧のドキュメントを取得
					doc = obj.getPortalPage(accessPageNum);
					// ドキュメントをパースオブジェクトにセット
					parse.setDoc(doc);
					// パースの実行
					parse.doParse();
					// パース結果をHtmlHashオブジェクトとして返却
					hash = parse.getHash();

					for(int i=0;i<10;i++){
						// List1つ分の情報オブジェクトのインスタンスを取得
						header = new HeaderInfo(
								hash[i].title, hash[i].org, hash[i].date, hash[i].type,
								accessPageNum, i);
						
						// ArrayListに追加
						if(hash[i].type.equals("univ")) { univTitles.add(header); }
						else if(hash[i].type.equals("message")) { messageTitles.add(header); }
						
						// 最新日時の更新
						if(accessPageNum == 0 && i == 0) { setLatestNewsTime(hash); }
						
					}
				}
				
				handler.post(new Runnable() {
					@Override
					public void run() {
						final ListLocation location = getListPosition();
						setAdapter();
						setListPosition(location);
						
						_isRunningListViewThread = false;
						
					}
				});
				
			}catch(Exception e) {
				e.printStackTrace();
				handler.post(new Runnable() {
					@Override
					public void run() {
						// TODO 自動生成されたメソッド・スタブ
		        		showToast("更新に失敗しました", 0);
					}
				});
				
			}
	        try{
	        	handler.post(new Runnable(){
	        		@Override
	        		public void run(){
				        // 回転アニメーションを終了
			        	rotate.cancel();
			        	rotate.end();
			        	rotate = null;
		                // 更新ボタンをアクティブにする
			        	btn_reload.setEnabled(true);

	        		}
	        	});
	        	
	        }catch(Exception e){
	        	Log.e("FloatButton-End-Exception", e.toString());
	        	
	        }
				
	        
		}
			
	}
	
    private void addViewContents(){
		// ListViewのポジションを取得
    	if(!_isRunningListViewThread){
    		_isRunningListViewThread = true;
        	
        	if (!_isFullAdded) {
            	AddViewThread th = new AddViewThread(true);
        		th.start();
        		
        	} else {
        		showToast("記事はこれ以上ありません", 0);
        		
        	}
    		
    	}
    	
    }
    
    private class AddViewThread extends Thread {
    	private boolean isShowDialog;
    	
    	public AddViewThread(boolean isShowDialog){
    		this.isShowDialog = isShowDialog;
        	if(isShowDialog){
        		progressDialog.setMessage("データ追加中...");
            	progressDialog.show();
        		
        	}
    		
    	}
		
    	@Override
		public void run(){
			try {
				HtmlHash[] hash = new HtmlHash[10];
				Document doc;
				HeaderInfo header;
	        	// Jsoupを用いたログイイン及びパース処理を行うオブジェクトのインスタンスを生成
				JsoupLogin obj = new JsoupLogin();
				JsoupParse parse = new JsoupParse();
				
	        	// クッキーを渡す
				obj.setCookies(cookies);
				
				loadPage.end ++;
				
				// 指定ページのニュース一覧のドキュメントを取得
				doc = obj.getPortalPage(loadPage.end);
				// ドキュメントをパースオブジェクトにセット
				parse.setDoc(doc);
				// パースの実行
				parse.doParse();
				// パース結果をHtmlHashオブジェクトとして返却
				hash = parse.getHash();

				for(int i=0;i<10;i++){
					// List1つ分の情報オブジェクトのインスタンスを取得
					header = new HeaderInfo(
							hash[i].title, hash[i].org, hash[i].date, hash[i].type,
							loadPage.end, i);
					
					// ArrayListに追加
					if(hash[i].type.equals("univ")) { univTitles.add(header); }
					else if(hash[i].type.equals("message")) { messageTitles.add(header); }
					
					// 最新日時の更新
					if(loadPage.end == 0 && i == 0) { setLatestNewsTime(hash); }
					
				}
				
				handler.post(new Runnable() {
					@Override
					public void run() {
						final ListLocation location = getListPosition();
						setAdapter();
						setListPosition(location);
						
						_isRunningListViewThread = false;
						
						/*int num = univTitles.size()+messageTitles.size();
						Toast.makeText(context, "Lists Number is #"+num, Toast.LENGTH_SHORT).show();*/
						
					}
				});
				
			}catch(ArrayIndexOutOfBoundsException e){
				_isFullAdded = true;
				
    		}catch(Exception e) {
				e.printStackTrace();
				handler.post(new Runnable() {
					@Override
					public void run() {
						// TODO 自動生成されたメソッド・スタブ
		        		showToast("更新に失敗しました", 0);
					}
				});
				
			}
			if(isShowDialog){
				// プロセスダイアログを非表示にする
				handlerPlog.sendEmptyMessage(0);
				
			}
			
		}
    	
    }
    
    /**
     * 
     * @param position：クリックポジション
     */
    private void loadViewDetailContent(int position){
    	progressDialog.setMessage("データ取得中...");
    	progressDialog.show();
    	final int onItemClick_Position = position;
    	LoadDetailThread th;

		if(univFragment.isMenuVisible()){
			// 大学からのリスト（右側）クリック時
	        th = new LoadDetailThread(onItemClick_Position, univTitles);
		}else{
			// 伝言のリスト（左側）クリック時
	        th = new LoadDetailThread(onItemClick_Position, messageTitles);
		}
		
    	th.start();
    	
        
    }
    
    private class LoadDetailThread extends Thread {
        private FragmentManager manager = getSupportFragmentManager();
        private PortalDetailDialog portalDetailDialog = new PortalDetailDialog();
        private int position;
        private ArrayList<HeaderInfo> arrayList;
        
        public LoadDetailThread(int position, ArrayList<HeaderInfo> arrayList){
        	this.arrayList = arrayList;
        	this.position = position;
        }
        
    	@Override
    	public void run(){
    		
        	JsoupLogin obj = new JsoupLogin();
        	JsoupParse_Detail detail = new JsoupParse_Detail();
        	HtmlHash_Detail hash = new HtmlHash_Detail();
        	
        	// クッキーを渡す
        	obj.setCookies(cookies);
        	
        	try {
        		final int index = arrayList.get(position).linkNum;
        		Document doc = obj.getPortalPage(arrayList.get(position).pageNum);
        		doc = obj.getDetailPage(doc, index);
				detail.setDoc(doc);
				detail.doParse();
				hash = detail.getHash();
				
			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				Log.e("Exception(DetailDialog)", e.toString());
			}
        	
        	try {
		        portalDetailDialog.setDetailText(hash.title,hash.date,hash.detail,hash.check_date);
		        portalDetailDialog.show(manager, "portal_detail_dialog");
        	} catch (Exception e) {
        		handler.post(new Runnable() {
					@Override
					public void run() {
						// TODO 自動生成されたメソッド・スタブ
						showToast("セッションタイムアウト：接続に失敗しました", 0);
					}
				});
        	}
        	// プログレスダイアログ終了
			handlerPlog.sendEmptyMessage(0);
			
    	}
    	
    }
	
	/**
	 * hash内の最も新しいデータの日時をSharedPreferencesに登録
	 * @param hash
	 * @throws ParseException
	 */
	@SuppressLint("SimpleDateFormat")
	private void setLatestNewsTime(HtmlHash[] hash) throws ParseException{
			SP sp = new SP(SharedPreferenceFileName, this);
			String saved_latest_date_time = sp.readStr(LatestNewsTimeValue);
			String loaded_latest_date_time = hash[0].date.replaceAll(" Date : ", "").replaceAll("\n", "");
			
			if(saved_latest_date_time != ""){
				// -------------------- セーブデータがある時 --------------------
				Timestamp saved_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
														.parse(saved_latest_date_time).getTime());
				Timestamp loaded_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
														.parse(loaded_latest_date_time).getTime());
				
				if(saved_timestamp.compareTo(loaded_timestamp) > 0){
					// 保存されているスタンプの方が最近の時
					//Log.d("now comp timestamp", saved_timestamp+", saved_timestampの方が遅い");
				}else if(saved_timestamp.compareTo(loaded_timestamp) == 0){
					// 保存されているスタンプと読み込んだスタンプが同じ時
				}else{
					// 読み込んだスタンプの方が最近の時
					//Log.d("now comp timestamp", saved_timestamp+", saved_timestampの方が早い");
					sp.write(LatestNewsTimeValue, loaded_latest_date_time);
				}
				
			}else{
				// -------------------- セーブデータがない時 --------------------
				sp.write(LatestNewsTimeValue, loaded_latest_date_time);
				
			}
			
			
	}
	
	private class LoadPage{
		public int start;
		public int end;
		
		public LoadPage(){
			clear();
			
		}
		public void clear(){
			start = 0;
			end = 0;
			
		}
		
	}
	
	
    
}