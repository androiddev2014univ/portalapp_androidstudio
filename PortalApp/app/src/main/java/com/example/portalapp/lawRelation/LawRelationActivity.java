package com.example.portalapp.lawRelation;

import com.example.portalapp.R;
import com.example.portalapp.library.Access;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class LawRelationActivity extends ActionBarActivity implements OnClickListener {
	TextView textHeader, sqlchipher_ReadMe, sqlchipher_License, jsoup_ReadMe;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_law_relation);
		
		Access access = new Access(this);
		textHeader = (TextView) findViewById(R.id.textHeaderMsg);
		sqlchipher_ReadMe = (TextView) findViewById(R.id.textReadMe_sqlcipher);
		sqlchipher_License = (TextView) findViewById(R.id.textLicense_sqlcipher);
		jsoup_ReadMe = (TextView) findViewById(R.id.textReadMe_Jsoup);

		textHeader.setText("\n以下に、本プログラムで用いているライブラリ（SqlChipher及びJsoup）のライセンス情報を示す"+"\n\n");
		sqlchipher_ReadMe.setText("\n\n"+access.getAssetsFile("", "sqlcipher/README")
				+"\n\n---------------------------------------------------------------------------------------------\n\n");
		sqlchipher_License.setText(access.getAssetsFile("", "sqlcipher/SQLCIPHER_LICENSE")+"\n\n\n\n\n");
		jsoup_ReadMe.setText(
				"=============================================================================================\n\n"
				+access.getAssetsFile("", "jsoup/README")
				+"\n\nURL : http://jsoup.org/license\n\n\n");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		
	}

}
