package com.example.portalapp.serviceBackground;

import com.example.portalapp.R;
import com.example.portalapp.library.InitialRunning;
import com.example.portalapp.library.Plan;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/*
 * Android OS起動時に実行するクラス
 */
public class StartupReceiver extends BroadcastReceiver {
    private static final String TAG = "StartupReceiver";
	private String SharedPreferenceFileName;
	private String isNotifiTagKey;
	private String intervalTagKey;
    private Context context;
    // 日本-イギリスの時差
    private static long Interval_UK2JP = 9 * 60 * 60 * 1000;
    private Plan plan;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO 自動生成されたメソッド・スタブ
		this.context = context;
		
		InitialRunning initTask = new InitialRunning(this.context);
		initTask.InitialRun();
		
		SharedPreferenceFileName = this.context.getString(R.string.shared_preferences_file_name_notifi);
		isNotifiTagKey = this.context.getString(R.string.shared_preferences_notifi_tag_name_isnotifi);
		intervalTagKey = this.context.getString(R.string.shared_preferences_notifi_tag_name_interval_sig);
		plan = new Plan(this.context, SharedPreferenceFileName, isNotifiTagKey, intervalTagKey, Interval_UK2JP);
		plan.refreshSchedule();
        
	}
	
}
