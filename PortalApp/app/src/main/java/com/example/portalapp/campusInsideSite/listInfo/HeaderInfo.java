package com.example.portalapp.campusInsideSite.listInfo;

public class HeaderInfo{
	public String title;
	public String author;
	public String date;
	public String type;
	public int pageNum;
	public int linkNum;
	
	public void add(
			String title, String author, String date, String type,
			int pageNum, int linkNum
	) {
		this.title=title;
		this.author=author;
		this.date=date;
		this.type=type;
		this.pageNum=pageNum;
		this.linkNum=linkNum;
		
	}
	public void clear(){
		this.add(null, null, null, null, 0, 0);
		
	}
	
	public HeaderInfo(){
		this.clear();
		
	}
	public HeaderInfo(
			String title, String author, String date, String type,
			int pageNum, int linkNum
	){
		this.add(title, author, date, type, pageNum, linkNum);
		
	}
	
}
