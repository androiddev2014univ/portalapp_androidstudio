package com.example.portalapp.settings.notifiinfo;

import java.util.List;

import com.example.portalapp.R;
import com.example.portalapp.library.Plan;
import com.example.portalapp.library.SP;
import com.example.portalapp.serviceBackground.CheckLatestMessage;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class NotifiInfoActivity extends ActionBarActivity implements OnClickListener {
	private static String TAG = "NotifiInfoActivity";
	private static String SharedPreferenceFileName = "notifi_pref";
	private static String isNotifiTagKey = "isnotifi";
	private static String intervalTagKey = "interval_sig";
	private static String isServiceNotificationKey = "isServiceNotification";
    // 日本-イギリスの時差
    private final static long Interval_UK2JP = 9 * 60 * 60 * 1000;
    private Plan plan;
	
	Button changeBtn, stopServiceBtn, startServiceBtn;
	ToggleButton isNotifiBtn, isShowBgServiceBtn;
	TextView interval_time_title, ctrl_service_title, isShowBgService_title;
	TextView now_interval_text, isNotifi_text, now_check_showBar_text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifiinfo);
		
		plan = new Plan(this, SharedPreferenceFileName, isNotifiTagKey, intervalTagKey, Interval_UK2JP);
		
		changeBtn = (Button) findViewById(R.id.change);
		changeBtn.setOnClickListener(this);
		stopServiceBtn = (Button) findViewById(R.id.stopService);
		stopServiceBtn.setOnClickListener(this);
		startServiceBtn = (Button) findViewById(R.id.startService);
		startServiceBtn.setOnClickListener(this);
		interval_time_title = (TextView) findViewById(R.id.interval_time_title);
		ctrl_service_title = (TextView) findViewById(R.id.ctrl_service_title);
		isShowBgService_title = (TextView) findViewById(R.id.checkShowBar_text);
		now_interval_text = (TextView) findViewById(R.id.now_interval_time);
		isNotifi_text = (TextView) findViewById(R.id.isNotifi_text);
		now_check_showBar_text = (TextView) findViewById(R.id.now_check_showBar_text);
		isNotifiBtn = (ToggleButton) findViewById(R.id.toggleNotifi);
		isNotifiBtn.setOnClickListener(this);
		isShowBgServiceBtn = (ToggleButton) findViewById(R.id.toggleIsShow);
		isShowBgServiceBtn.setOnClickListener(this);
		
		// initRefresh();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public void onResume(){
		super.onResume();
		
		initRefresh();
	}
	
	public void initRefresh(){
		SP sp = new SP(SharedPreferenceFileName, this);
		boolean flg = sp.readBool(isNotifiTagKey);
		boolean isShowBgService = sp.readBool(isServiceNotificationKey);
		int sig = sp.readInt(intervalTagKey);
		
		isNotifiBtn.setChecked(flg);
		isShowBgServiceBtn.setChecked(isShowBgService);
		setText_now_interval_text(sig);
		setText_isNotifi_text(flg);
		setNow_check_showBar_text(isShowBgService);
		
		if(flg){
			interval_time_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
			ctrl_service_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
			isShowBgService_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
		}else{
			interval_time_title.setTextColor(getResources().getColor(R.color.white));
			ctrl_service_title.setTextColor(getResources().getColor(R.color.white));
			isShowBgService_title.setTextColor(getResources().getColor(R.color.white));
		}
		
	}
	
	public void setText_now_interval_text(int sig){
		switch(sig){
		case 0: now_interval_text.setText("設定されている間隔：５分"); break;
		case 1: now_interval_text.setText("設定されている間隔：１５分"); break;
		case 2: now_interval_text.setText("設定されている間隔：３０分"); break;
		case 3: now_interval_text.setText("設定されている間隔：１時間"); break;
		case 4: now_interval_text.setText("設定されている間隔：６時間"); break;
		case 5: now_interval_text.setText("設定されている間隔：半日（１２時間）"); break;
		case 6: now_interval_text.setText("設定されている間隔：１日（２４時間）"); break;
		case 7: now_interval_text.setText("設定されている間隔：３日（７２時間）"); break;
		default: now_interval_text.setText("設定されている間隔：設定されていません"); break;
		}
	}
	
	public void setText_isNotifi_text(boolean flg){
		if(flg){
			isNotifi_text.setText("アクティブ状態：通知されます");
		}else{
			isNotifi_text.setText("スリープ状態：通知されません");
		}
	}
	
	public void setNow_check_showBar_text(boolean flg){
		if(flg){
			now_check_showBar_text.setText("サービス起動時に通知されます");
		}else{
			now_check_showBar_text.setText("サービスが起動しても通知されません");
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		
		if(v.getId() == R.id.change){
			Dialog dialog = onCreateDialogSingleChoice();
			dialog.show();
		}else if(v.getId() == R.id.toggleNotifi){
			SP sp = new SP(SharedPreferenceFileName, this);
			if(isNotifiBtn.isChecked()){
				//Toast.makeText(this, "true", Toast.LENGTH_SHORT).show();
				sp.write(isNotifiTagKey, true);
				setText_isNotifi_text(true);
				interval_time_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
				ctrl_service_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
				isShowBgService_title.setTextColor(getResources().getColor(R.color.background_service_active_color));
				
				plan.refreshSchedule();
			}else{
				//Toast.makeText(this, "false", Toast.LENGTH_SHORT).show();
				sp.write(isNotifiTagKey, false);
				setText_isNotifi_text(false);
				interval_time_title.setTextColor(getResources().getColor(R.color.white));
				ctrl_service_title.setTextColor(getResources().getColor(R.color.white));
				isShowBgService_title.setTextColor(getResources().getColor(R.color.white));
				
				plan.refreshSchedule();
			}
		}else if(v.getId() == R.id.toggleIsShow){
			SP sp = new SP(SharedPreferenceFileName, this);
			if(isShowBgServiceBtn.isChecked()){
				sp.write(isServiceNotificationKey, true);
				setNow_check_showBar_text(true);
				
			}else{
				sp.write(isServiceNotificationKey, false);
				setNow_check_showBar_text(false);
				
			}
		}else if(v.getId() == R.id.stopService){
			if(isServiceRunnig(CheckLatestMessage.class.getName())){
				Intent intent = new Intent(this, com.example.portalapp.serviceBackground.CheckLatestMessage.class);
				stopService(intent);
			}else{
				Toast.makeText(this, "サービスは起動していません", Toast.LENGTH_SHORT).show();
			}
		}else if(v.getId() == R.id.startService){
			SP sp = new SP(SharedPreferenceFileName, this);
			if(sp.readBool(isNotifiTagKey)){
				if(!isServiceRunnig(CheckLatestMessage.class.getName())){
					plan.setSchedule(); // 「重要」スケージュールをセットしなおしてプロセスが重複するのを防ぐ
					Toast.makeText(this, "約３秒後にサービスが起動します", Toast.LENGTH_SHORT).show();
					new Thread(){
						@Override
						public void run(){
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								// TODO 自動生成された catch ブロック
								e.printStackTrace();
							}
							Intent intent = new Intent(NotifiInfoActivity.this,
									com.example.portalapp.serviceBackground.CheckLatestMessage.class);
							startService(intent);
						}
					}.start();
					
				}else{
					Toast.makeText(this, "サービスは既に起動しています", Toast.LENGTH_SHORT).show();
				}
			}else{
				Toast.makeText(this, "通知がOFFになっているため、サービスが起動できません", Toast.LENGTH_LONG).show();
			}
		}
		
	}
	
	private int singleChoicePosition = 0;
	private Dialog onCreateDialogSingleChoice(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
		CharSequence[] array = {
				"５分", "１５分", "３０分", "１時間", "６時間", "半日（１２時間）", "１日（２４時間）", "３日（７２時間）"
				};
		singleChoicePosition = 0;
		
		SP sp = new SP(SharedPreferenceFileName, this);
		
		builder.setTitle("実行間隔を選択")
		.setSingleChoiceItems(array, sp.readInt(intervalTagKey), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO 自動生成されたメソッド・スタブ
				singleChoicePosition = which;
			}
		});
		builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO 自動生成されたメソッド・スタブ
				//Toast.makeText(NotifiInfoActivity.this , singleChoicePosition+"", Toast.LENGTH_LONG).show();
				SP sp = new SP(SharedPreferenceFileName, NotifiInfoActivity.this);
				sp.write(intervalTagKey, singleChoicePosition);
				
				setText_now_interval_text(singleChoicePosition);
				
				plan.refreshSchedule();
			}
		});
		builder.setNegativeButton("Cancel", null);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		
		return dialog;
	}
    
    private boolean isServiceRunnig(String className) {
        ActivityManager am = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
        List<RunningServiceInfo> listServiceInfo = am.getRunningServices(Integer.MAX_VALUE);
         
        Log.i(TAG + "isServiceRunnig", "Search Start: " + className);
        for (RunningServiceInfo curr : listServiceInfo) {
            Log.i(TAG + "isServiceRunnig", "Check: " + curr.service.getClassName());
            if (curr.service.getClassName().equals(className)) {
                Log.i(TAG + "isServiceRunnig", ">>>>>>FOUND!");
                return true;
            }
        }
        return false;
    }

}
