package com.example.portalapp.library.sqlite.table;

import java.util.Calendar;
import android.content.ContentValues;
import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * 
 * @author teamMykata
 *
 */
public class Post {
	@SuppressWarnings("unused")
	private static final String TAG = Post.class.getSimpleName();
	public static final String TABLE_NAME = Post.class.getSimpleName();
	private SQLiteDatabase mDatabase;
	public enum Column{
		// @formatter:off
		ID("_id", "INTEGER PRIMARY KEY AUTOINCREMENT"), // as long
		USERID("userid", "TEXT"),
		PASSWD("passwd", "TEXT"),
		CREATED("created", "INTEGER"), // as long
		MODIFIED("modified", "INTEGER")
		;
		// @formatter:on

        private String mActualColumnName;
        private String mType;

        /**
         * @param actualColumnnName
         * @param type
         */
        private Column(String actualColumnnName, String type) {
            this.mActualColumnName = actualColumnnName;
            this.mType = type;
        }

        /**
         * @return
         */
        public String getColumnName() {
            return this.mActualColumnName;
        }

        /**
         * @return
         */
        public String getType() {
            return this.mType;
        }
	}
    /**
     * @param database
     */
    public static void createTable(SQLiteDatabase database) {
        // @formatter:off
        StringBuilder builder = new StringBuilder()
                .append("CREATE TABLE IF NOT EXISTS ").append(TABLE_NAME).append("(");
        // @formatter:on
        for (Column column : Column.values()) {
            builder.append(column.getColumnName()).append(" ").append(column.getType()).append(",");
        }
        builder.replace(builder.length() - 1, builder.length(), ")");
        String sql = builder.toString();
        database.execSQL(sql);
    }
    /**
     * @param username
     * @param password
     * @param database
     */
    public static void insertValues(String username,String password,SQLiteDatabase database) {
        Post post = new Post(database);
        post.insert(username,password);
    }
    
    /**
     * @param database
     */
    public Post(SQLiteDatabase database) {
        this.mDatabase = database;
    }
    /**
     * @param userid
     * @param passwd
     * @return
     */
    public long insert(String userid, String passwd) {

        long now = Calendar.getInstance().getTimeInMillis();
        ContentValues values = new ContentValues();
        values.put(Column.USERID.getColumnName(), userid);
        values.put(Column.PASSWD.getColumnName(), passwd);
        values.put(Column.CREATED.getColumnName(), now);
        values.put(Column.MODIFIED.getColumnName(), now);
        return this.mDatabase.insert(TABLE_NAME, null, values);
    }
    /**
     * @return
     */
    public Cursor selectAll() {
        String[] columns = null;
        String selection = null;
        String[] selectionArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        return this.mDatabase.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having,
                orderBy);
    }
}

