package com.example.portalapp.library.sqlite;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.example.portalapp.library.sqlite.table.Post;
import com.example.portalapp.library.sqlite.OpenHelper;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;

public class ReadDB extends FragmentActivity implements LoaderCallbacks<Cursor>, TextWatcher{
	private String username_info, passwd_info;
	private OpenHelper helper;
	private static SQLiteDatabase db;
	private Context context;
	public ReadDB(Context context){
		this.context = context;
	}
	static {
		System.loadLibrary("stlport_shared");
		System.loadLibrary("Portalapp");
	}
	public static native String getApplicationKey(Context context);
	// 端末内Databaseを復号した後、ユーザ名とパスワードを検索・抽出する
	public void init(){
        helper = new OpenHelper(this.context);
        // -------------------------------------------------------------------------------------------
        helper.setSaltKey(getApplicationKey(this.context)); // サルトキーをセット
        // -------------------------------------------------------------------------------------------
        db = helper.getReadableDatabase();
        Cursor c = db.query(Post.class.getSimpleName(), new String[] {
        	Post.Column.USERID.getColumnName(), Post.Column.PASSWD.getColumnName()
        }, null, null, null, null, null, null);
        c.moveToFirst();
        username_info = c.getString(0);
        passwd_info = c.getString(1);
        c.close();
        db.close();
	}
	public String getUsername(){
		return username_info;
	}
	public String getPassword(){
		return passwd_info;
	}
    @SuppressWarnings("unused")
	private static class ListItemLoader extends AsyncTaskLoader<Cursor> {

        public ListItemLoader(Context context) {
            super(context);
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.support.v4.content.Loader#onStartLoading()
         */
        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            this.forceLoad();
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.support.v4.content.AsyncTaskLoader#loadInBackground()
         */
        @Override
        public Cursor loadInBackground() {
            Context context = this.getContext();
            OpenHelper openHelper = OpenHelper.getInstance(context);
            SQLiteDatabase database = openHelper.getWritableDatabase();
            Post post = new Post(database);
            return post.selectAll();
            
        }
        
    }
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	@Override
	public void afterTextChanged(Editable s) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
}
