package com.example.portalapp.campusInsideSite.extJsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.example.portalapp.campusInsideSite.listInfo.HtmlHash_Detail;


public class JsoupParse_Detail {
	private HtmlHash_Detail hash_detail = new HtmlHash_Detail();
	private Document doc;
	
	public void setDoc(Document doc) {
		this.doc = doc;
		
	}
	public void doParse() {
		Element element = this.doc.getElementsByTag("form").first();
		String[] parts_hr = element.html().split("<hr />");
		String[] parts_br = parts_hr[2].split("<br />");
		String message ="";
		String date ="";
		String check_date ="";
		for(int i=0;i<parts_br.length -1;i++) {
			if(i == parts_br.length -3) {
				date = parts_br[i];
			}else if(i == parts_br.length -2) {
				check_date = parts_br[i];
			}else {
				message += parts_br[i];
			}
		}
		parts_br = parts_hr[1].split("<br />");
		hash_detail.set(
				parts_br[1],
				date.replaceAll("\n", ""),
				"",
				parts_br[0].replaceAll("\n", ""),
				"",
				message,
				check_date.replaceAll("\n", "")
				);

	}
	public HtmlHash_Detail getHash() {
		return this.hash_detail;
	}

}
