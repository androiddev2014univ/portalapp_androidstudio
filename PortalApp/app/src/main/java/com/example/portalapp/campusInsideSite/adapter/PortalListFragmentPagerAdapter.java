package com.example.portalapp.campusInsideSite.adapter;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.fragment.MessagePortalListFragment;
import com.example.portalapp.campusInsideSite.fragment.UnivPortalListFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class PortalListFragmentPagerAdapter extends FragmentPagerAdapter {
	private Context context;

    public PortalListFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
        case 0:
            return UnivPortalListFragment.getInstance();
        default:
            return MessagePortalListFragment.getInstance();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
        case 0:
            return this.context.getString(R.string.portal_list_fragment_tab_univ);
        default:
            return this.context.getString(R.string.portal_list_fragment_tab_message);
        }    
        
    }
}
