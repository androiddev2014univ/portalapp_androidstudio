package com.example.portalapp.settings.userinfo;

import java.util.Stack;

import com.example.portalapp.R;
import com.example.portalapp.library.sqlite.OpenHelper;
import com.example.portalapp.library.sqlite.table.Post;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserInfoActivity extends ActionBarActivity implements LoaderCallbacks<Cursor>, TextWatcher{
	//private static boolean isLoaded =false;
	static {
		//if(!isLoaded){
			System.loadLibrary("stlport_shared");
			System.loadLibrary("Portalapp");
			//isLoaded =true;
		//}
	}
	public static native String getApplicationKey(Context context);
	
	static OpenHelper helper;
	static SQLiteDatabase db;
    EditText username;
    EditText password;
    EditText passwordConfirm;
    TextView userid;
    Handler dbUpdateHandler;
    Handler dbDeleteTableHandler;
    Handler dbLoadHandler;
    ProgressDialog progressDialog;
    
    private OnClickListener update_button_ClickListener = new OnClickListener(){
    	public void onClick(View v){update_button_Click(v);}
    };
    private OnClickListener delete_button_ClickListener = new OnClickListener(){
    	public void onClick(View v){delete_button_Click(v);}
    };
    /*
     * (non-Javadoc)
     * 
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @SuppressLint("NewApi") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
        
        progressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("データ取得中...");
        progressDialog.show(); // start

        dbUpdateHandler = new Handler();
        dbDeleteTableHandler = new Handler();
        dbLoadHandler = new Handler();
        
        Button button_update = (Button) this.findViewById(R.id.buttonUpdate);
        Button button_delete = (Button) this.findViewById(R.id.buttonDelete);
        button_update.setOnClickListener(update_button_ClickListener);
        button_delete.setOnClickListener(delete_button_ClickListener);
        username = (EditText) findViewById(R.id.editUsername);
        password = (EditText) findViewById(R.id.editPasswd);
        passwordConfirm = (EditText) findViewById(R.id.editPasswordConfirm);
        userid = (TextView) findViewById(R.id.textViewUserId);
        username.addTextChangedListener(this);
        password.addTextChangedListener(this);
        passwordConfirm.addTextChangedListener(this);
    	
        new Thread(){
        	private Cursor c;
        	private String name, passwd;
        	@Override
        	public void run(){
        		helper = new OpenHelper(getApplicationContext());
                // -------------------------------------------------------------------------------------------
                helper.setSaltKey(getApplicationKey(getApplicationContext())); // サルトキーをセット
                // -------------------------------------------------------------------------------------------
                db = helper.getReadableDatabase();
                c = db.query(Post.class.getSimpleName(), new String[] {
                	Post.Column.USERID.getColumnName(), Post.Column.PASSWD.getColumnName()
                }, null, null, null, null, null, null);
                c.moveToFirst();
                name = c.getString(0);
                passwd = c.getString(1);
                c.close();
                db.close();
                
        		dbLoadHandler.post(new Runnable(){
        			@Override
        			public void run(){
        				username.setText(name);
        		        userid.setText(name);
        		        password.setText(passwd);
        		        passwordConfirm.setText(passwd);
        		        
        			}
        		});
        		
        		handlerPlog.sendEmptyMessage(0); // end
        	}
        }.start();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
    }
    @SuppressLint("HandlerLeak") private Handler handlerPlog = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// ロード終了
			progressDialog.dismiss();
		}
	};
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    private boolean strcmp(String s1, String s2){
    	if(s1.equals(s2)){
    		return true;
    	}
    	return false;
    }    
	private void update_button_Click(View v) {
		boolean isError= false;
		//final String name = username.getText().toString();
		final String pass = password.getText().toString();
		final String passconfirm = passwordConfirm.getText().toString();
		final int name_len = username.getText().length();
		final int pass_len = password.getText().length();
		final int passconfirm_len = passwordConfirm.getText().length();
		
		if(name_len == 0){
			username.setError("文字を入力して下さい");
			isError = true;
		}else{username.setError(null);}
		if(pass_len == 0){
			password.setError("文字を入力して下さい");
			isError = true;
		}else{password.setError(null);}
		if(passconfirm_len == 0){
			passwordConfirm.setError("文字を入力して下さい");
			isError = true;
		}else if(!strcmp(pass, passconfirm)){
			passwordConfirm.setError("パスワードが一致していません");
			isError = true;
		}else{passwordConfirm.setError(null);}
		
		if(!isError){ // 入力が正しい場合
	        progressDialog.setMessage("データ更新中...");
	        progressDialog.show(); // start
			Thread thread = new Thread(setDataRunnable);
			thread.start();
		}
	}
	Runnable setDataRunnable = new Runnable(){
		private int ret;
		private String tmp;
		public void run() {
			// TODO 自動生成されたメソッド・スタブ
			ContentValues val = new ContentValues();
			val.put(Post.Column.USERID.getColumnName(), username.getText().toString());
			val.put(Post.Column.PASSWD.getColumnName(), password.getText().toString());
			
			String whereClause = Post.Column.ID.getColumnName() + " = ?";
			String whereArgs[] = new String[1];
			whereArgs[0] = "1";
			
			db = helper.getWritableDatabase();
			//int ret;
			try {
				ret = db.update(Post.class.getSimpleName(), val, whereClause, whereArgs);
			} finally {
				db.close();
			}
			if (ret == -1){
				//Toast.makeText(UserInfoActivity.this, "Updateに失敗しました", Toast.LENGTH_SHORT).show();
			} else {
				//Toast.makeText(UserInfoActivity.this, "Updateに成功しました", Toast.LENGTH_SHORT).show();
				
		        db = helper.getReadableDatabase();
		        Cursor c = db.query(Post.class.getSimpleName(), new String[] {
		        	Post.Column.USERID.getColumnName(), Post.Column.PASSWD.getColumnName()
		        }, null, null, null, null, null, null);
		        c.moveToFirst();
		        //userid.setText(c.getString(0));
		        tmp = c.getString(0);
		        c.close();
		        db.close();
			}
	        dbUpdateHandler.post(new Runnable(){
	        	@Override
	        	public void run(){
	        		if(ret == -1){
	        			Toast.makeText(UserInfoActivity.this, "Updateに失敗しました", Toast.LENGTH_SHORT).show();
	        		}else{
	        			Toast.makeText(UserInfoActivity.this, "Updateに成功しました", Toast.LENGTH_SHORT).show();
	        			userid.setText(tmp);
	        		}
	        	}
	        });
	        handlerPlog.sendEmptyMessage(0); // end
		}
	};
	@SuppressLint("NewApi") private void delete_button_Click(View v) {
		// Deleteと言っても上書きをしいてるだけです
		AlertDialog.Builder alertDlg = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
		alertDlg.setTitle("データの削除");
		alertDlg.setMessage("DB内の保存データを削除しますか？");
		alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// OKボタンクリック時の処理
				dialog.dismiss();
		        progressDialog.setMessage("データ削除中...");
		        progressDialog.show(); // start
				Thread thread = new Thread(deleteDataRunnable);
				thread.start();
			}
		});
		alertDlg.setNegativeButton("Cancel", null);
		alertDlg.create().show();
	}
	Runnable deleteDataRunnable = new Runnable(){
		private int ret;
		private Stack<String> stack = new Stack<String>();
		public void run() {
			// TODO 自動生成されたメソッド・スタブ
			ContentValues val = new ContentValues();
			/*
			val.put(Post.Column.USERID.getColumnName(), "sample_user");
			val.put(Post.Column.PASSWD.getColumnName(), "sample_passwd");
			*/
			val.put(Post.Column.USERID.getColumnName(), "");
			val.put(Post.Column.PASSWD.getColumnName(), "");
			
			String whereClause = Post.Column.ID.getColumnName() + " = ?";
			String whereArgs[] = new String[1];
			whereArgs[0] = "1";
			
			db = helper.getWritableDatabase();
			//int ret;
			try {
				ret = db.update(Post.class.getSimpleName(), val, whereClause, whereArgs);
			} finally {
				db.close();
			}
			if (ret == -1){
				//Toast.makeText(UserInfoActivity.this, "Deleteに失敗しました", Toast.LENGTH_SHORT).show();
			} else {
				//Toast.makeText(UserInfoActivity.this, "Deleteに成功しました", Toast.LENGTH_SHORT).show();
				
		        db = helper.getReadableDatabase();
		        Cursor c = db.query(Post.class.getSimpleName(), new String[] {
		        	Post.Column.USERID.getColumnName(), Post.Column.PASSWD.getColumnName()
		        }, null, null, null, null, null, null);
		        c.moveToFirst();
		        //username.setText(c.getString(0));
		        //userid.setText(c.getString(0));
		        //password.setText(c.getString(1));
		        //passwordConfirm.setText(c.getString(1));
		        stack.push(c.getString(1));
		        stack.push(c.getString(1));
		        stack.push(c.getString(0));
		        stack.push(c.getString(0));
		        c.close();
		        db.close();
			}
			dbDeleteTableHandler.post(new Runnable(){
				@Override
				public void run(){
					if(ret == -1){
						Toast.makeText(UserInfoActivity.this, "Deleteに失敗しました", Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(UserInfoActivity.this, "Deleteに成功しました", Toast.LENGTH_SHORT).show();
						username.setText(stack.pop());
						userid.setText(stack.pop());
						password.setText(stack.pop());
						passwordConfirm.setText(stack.pop());
					}
				}
			});
	        handlerPlog.sendEmptyMessage(0); // end
		}
	};
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO 自動生成されたメソッド・スタブ
		
	}
    @SuppressWarnings("unused")
	private static class ListItemLoader extends AsyncTaskLoader<Cursor> {

        public ListItemLoader(Context context) {
            super(context);
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.support.v4.content.Loader#onStartLoading()
         */
        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            this.forceLoad();
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.support.v4.content.AsyncTaskLoader#loadInBackground()
         */
        @Override
        public Cursor loadInBackground() {
            Context context = this.getContext();
            OpenHelper openHelper = OpenHelper.getInstance(context);
            SQLiteDatabase database = openHelper.getWritableDatabase();
            Post post = new Post(database);
            return post.selectAll();
        }
    }
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (username.getText().length() != 0){
			username.setError(null);
		}
		if (password.getText().length() != 0){
			password.setError(null);
			if (passwordConfirm.getText().length() != 0){
				passwordConfirm.setError(null);
			}
		}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}
	

}
