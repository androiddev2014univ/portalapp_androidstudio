package com.example.portalapp.license;

import com.example.portalapp.R;
import com.example.portalapp.library.Access;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class LicenseActivity extends ActionBarActivity {
	private TextView view;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_license);
		
		Access access = new Access(this);
		view = (TextView) findViewById(R.id.textView1);
		String readString = "";
		view.setText(""+access.getAssetsFile(readString, "PortalappLicense/LICENSE"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    

}
