package com.example.portalapp.settings;

import java.util.ArrayList;
import java.util.List;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.fragment.ConfigAlertDialogFragment;
import com.example.portalapp.settings.devinfo.DevInfoActivity;
import com.example.portalapp.settings.notifiinfo.NotifiInfoActivity;
import com.example.portalapp.settings.userinfo.UserInfoActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

@SuppressLint("NewApi")
public class PreferenceActivity extends ActionBarActivity implements OnItemClickListener
{	
	ListView mlist;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preference);
		
		mlist = (ListView) findViewById(R.id.preference_list);        

		initOfList(mlist);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mlist.setOnItemClickListener(this);
	}
	private void initOfList(ListView tmplist) {
		// TODO 自動生成されたメソッド・スタブ
		ArrayList<Title> tmp = new ArrayList<Title>();
		
		tmp.add(new Title("ユーザ情報","ログイン情報を設定します"));
		tmp.add(new Title("通知","通知のON・OFF、バックグラウンドサービス（CheckLatestMessage）の実行間隔の設定を行います"));
		tmp.add(new Title("開発者向けオプション","開発者向けの設定などを行います"));
		tmp.add(new Title("読み込み件数", "学内サイトにログインした後、最初に読み込む記事の件数を設定します"));
		tmp.add(new Title("その他",""));
		
		tmplist.setAdapter(new SettingAdapter(PreferenceActivity.this, tmp));
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO 自動生成されたメソッド・スタブ
		if(position == 0){ // ログイン情報を設定するアクティビティ
			Intent intent = new Intent(PreferenceActivity.this, UserInfoActivity.class);
			PreferenceActivity.this.startActivity(intent);
	    	
		}else if(position == 1){ // 通知設定のアクティビティ
			Intent intent = new Intent(this, NotifiInfoActivity.class);
			startActivity(intent);
			
		}else if(position == 2){ // 開発者向けオプションのアクティビティ
			Intent intent = new Intent(PreferenceActivity.this, DevInfoActivity.class);
			startActivity(intent);
					
		}else if(position == 3){
			ConfigAlertDialogFragment fm = new ConfigAlertDialogFragment(this);
			fm.show(getSupportFragmentManager(), "alert_dialog");
			
		}
		
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
	
	private class Title{
		String title;
		String subtitle;
		public Title(String string, String string2) {
			// TODO 自動生成されたコンストラクター・スタブ
			this.set(string, string2);
		}
		void set(String t1, String t2){
			this.title = t1;
			this.subtitle = t2;
		}
	}
	private class SettingAdapter extends ArrayAdapter<Title>{
		LayoutInflater inflater;
		public SettingAdapter(Context context, List<Title> obj){
			super(context, -1, obj);
			inflater = LayoutInflater.from(context);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent){
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.list_preference, parent, false);
			}
			Title obj = getItem(position);
			TextView title = (TextView) convertView.findViewById(R.id.textView_title);
			TextView subtitle = (TextView) convertView.findViewById(R.id.textView_subtitle);
			
			title.setText(obj.title);
			subtitle.setText(obj.subtitle);
			
			return convertView;
		}
	}

}
