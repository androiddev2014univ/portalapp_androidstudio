package com.example.portalapp.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.example.portalapp.R;
import com.example.portalapp.bustime.BusRoute;
import com.example.portalapp.bustime.BusTimeTable;
import com.example.portalapp.bustime.KeihanBusTimeTableParser;
import com.example.portalapp.bustime.BusRoute.Time;
import com.example.portalapp.library.SP;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.RemoteViews;
import android.widget.Toast;

public class BusTimeTableReloadService extends Service {
	private final static String SETTING_BUTTON_CLICK_ACTION = "SETTING_BUTTON_CLICK_ACTION";
	private final static String RELOAD_BUTTON_CLICK_ACTION = "RELOAD_BUTTON_CLICK_ACTION";
	private long cnt = 0;
	private Context context = this;
	private boolean isInit = true;
	
	private String SharedPreferenceFileName;
	private String typeSignalTagKey;
	private String KEIHANBUS_SCRIPT_URL;
	private String[] urls, terminal_list;
    private ArrayList<ArrayList<Integer>> routesIndexes;
    private TreeMap<String, String> busResult;
    private Handler handler = new Handler();
    SP sp;
	
    private Intent reloadButtonIntent, settingButtonIntent;
    private PendingIntent reloadPendingIntent, settingPendingIntent;
	private RemoteViews remoteViews;
	private AppWidgetManager manager;
	private ComponentName thisWidget;
	
	private void reloadTimeTable(){
		// 0-3
		int position = sp.readInt(typeSignalTagKey);;

        busResult.clear();
        loadBusData(position);
        if (position == 3) {
            loadBusData(4);
        }
        this.showBusData();
        
	}
	
	/**
     * 
     * @param index
     * @param isShowProgressDialog
     */
    private void loadBusData(final int index) {

        new Thread(new Runnable() {
            BusTimeTable table;
            
            @Override
            public void run() {
                try {
                    Document doc = Jsoup.connect(urls[index]).get();
                    Element htmlTable = doc.getElementById("weekday");
                    KeihanBusTimeTableParser parser = new KeihanBusTimeTableParser();
                    table = parser.parse(htmlTable);

                } catch (final Exception e) {
                	handler.post(new Runnable(){
                		@Override
                		public void run(){
	                        showToast("バスのデータの所得に失敗しました...\n" + e.toString(), true);
                			
                		}
                		
                	});

                }

                handler.post(new Runnable(){
                	@Override
                	public void run(){
        	            try {
        	                laodBusData(table, index);
        	                showBusData();
        	
        	            } catch (Exception e) {
        	                e.printStackTrace();
        	            }
                	}
                });
                
            }

        }).start();
    }
	
    private void showBusData() {

        String[] result = new String[5];
        int[] viewid = {
        		R.id.textBusTimeTable,
        		R.id.textBusTimeTable2,
        		R.id.textBusTimeTable3,
        		R.id.textBusTimeTable4,
        		R.id.textBusTimeTable5
        		};
        int cnt = 0;
        for (Map.Entry<String, String> e : busResult.entrySet()) {
            result[cnt] = e.getKey() + "" + e.getValue();// + "\n";
            
            cnt++;

            if (cnt == 5) {
                break;
            }
        }

        // 検索結果をテキストビューにセット
    	for(int i=0;i<viewid.length;i++){
    		remoteViews.setTextViewText(viewid[i], result[i]);
    	}

		// AppWidget更新
		thisWidget = new ComponentName(context, KeihanBusWidgetProvider.class);
		manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(thisWidget, remoteViews);
    }
    
    /**
     * 
     * @param table
     * @param index
     */
    private void laodBusData(BusTimeTable table, int index) {
        Calendar date = Calendar.getInstance();

        int nowHour = date.get(Calendar.HOUR_OF_DAY);
        int nowMinute = date.get(Calendar.MINUTE);

        for (int h = nowHour; h <= nowHour + 1; h++) {
            for (Integer routeIndex : routesIndexes.get(index)) {
                // System.out.println("route index = " + routeIndex);

                BusRoute busRoute = table.getRoute(routeIndex.intValue());
                Time time = busRoute.getTime(h);
                Time.Minute[] minutes = time.minutes;

                if (minutes == null) {
                    continue;
                }

                for (int i = 0; i < minutes.length; i++) {
                    if (h == nowHour && minutes[i].intValue() < nowMinute) {
                        continue;
                    }
                    
                    String option = "";
                    switch (index) {
                    case 0: option = terminal_list[0]; break;
                    case 1: option = terminal_list[1]; break;
                    case 2: option = terminal_list[2]; break;
                    case 3: option = terminal_list[3]; break;
                    case 4: option = terminal_list[4]; break;
                    default : option = ""; break;
                    }
                    busResult.put(
                    		String.format("%02d時%02d分", h,minutes[i].intValue()),
                    		option);
                    
                }
                

            }
        }

    }
    
    /**
     * 
     * @param msg
     * @param isShow
     */
    private void showToast(String msg, boolean isShow){
    	 if(isShow) Toast.makeText(this.context, msg+"", Toast.LENGTH_SHORT).show();
    	 
    }
    
    // 更新中のテキストビューを設定する
    private void setTextNowLoading(){
    	setTextView("Now Loading...");
		
    }
    
    /**
     * 
     * @param text
     */
    private void setTextView(final String text){
    	RemoteViews remoteViews;
    	AppWidgetManager manager;
    	ComponentName thisWidget;
    	
		// ビューを作成
		remoteViews = new RemoteViews(getPackageName(), R.layout.widgetprovider_keihanbus);
		// テキストをセットする
		remoteViews.setTextViewText(R.id.textBusTimeTable, text+"");
		// AppWidget更新
		thisWidget = new ComponentName(context, KeihanBusWidgetProvider.class);
		manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(thisWidget, remoteViews);
    	
    }
	
	// _________________________________________________________________________________________________________
	@Override
	public void onCreate(){
		super.onCreate();

		SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_bustime);
		typeSignalTagKey = getString(R.string.shared_preferences_bustime_tag_name_typeSignal);
		KEIHANBUS_SCRIPT_URL = getString(R.string.keihanbus_script_url);
		
        /*
         * 上から
         * 長尾 -> 情報科学部
         * 情報科学部 -> 長尾
         * 樟葉 -> 情報科学部
         * 情報科学部 -> 樟葉
         * 情報科学部 -> 樟葉
         * 
         */
        urls = new String[5];
        urls = getResources().getStringArray(R.array.query_lists);
        for(int i=0;i<5;i++){
        	urls[i] = KEIHANBUS_SCRIPT_URL + urls[i];
        }

        terminal_list = new String[5];
        terminal_list = getResources().getStringArray(R.array.bus_terminal_names);

        routesIndexes = new ArrayList<ArrayList<Integer>>();
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(0, 1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));

        busResult = new TreeMap<String, String>();

		sp = new SP(SharedPreferenceFileName, context);
        
	}
	
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		try {
			// RemoteViewのインスタンス取得
			remoteViews = new RemoteViews(getPackageName(), R.layout.widgetprovider_keihanbus);
			
			// 設定ボタンが押された時に発行されるインテントを準備
			settingButtonIntent = new Intent().setAction(SETTING_BUTTON_CLICK_ACTION);
			settingPendingIntent = PendingIntent.getService(context, 0, settingButtonIntent, 0);
			remoteViews.setOnClickPendingIntent(R.id.buttonBusTimeTableSetting, settingPendingIntent);
			
			// 更新ボタンが押された時に発行されるインテントを準備
			reloadButtonIntent = new Intent().setAction(RELOAD_BUTTON_CLICK_ACTION);
			reloadPendingIntent = PendingIntent.getService(context, 0, reloadButtonIntent, 0);
			remoteViews.setOnClickPendingIntent(R.id.buttonKeihanBusDetail, reloadPendingIntent);
			
			// 初期化処理
			if(isInit){
		        reloadTimeTable();
		        showToast("TextView Initialization", false);

				// 更新中のテキストをセット
				setTextNowLoading();
			
			}
			
			
			// 更新ボタンが押された時に発行されたインテントの場合
			if (RELOAD_BUTTON_CLICK_ACTION.equals(intent.getAction())) {
				showToast("ReloadButtonClicked", false);
				cnt ++;
				reloadTimeTable();

				// 更新中のテキストをセット
				setTextNowLoading();
				
				
				
			// 設定ボタンが押された時に発行されたインテントの場合
			}else if(SETTING_BUTTON_CLICK_ACTION.equals(intent.getAction())) {
				showToast("SettingButtonClicked", false);
				
				// 設定アクテビティを起動するためのインテントのインスタンスを生成
				Intent dialogIntent = new Intent(context, SettingAlertDialogActivity.class);
				final PendingIntent settingPendingIntent = PendingIntent.getActivity(context, 0, dialogIntent, 0);
				try {
					settingPendingIntent.send();
					
				} catch (CanceledException e) {
				    //e.printStackTrace();
					
				}
				
			}

			
			
			isInit = false;
		} catch (Exception e) {
			showToast("エラーが発生しました："+e.toString(), false);
			
		}
		
		showToast("Running Service #"+cnt, false);
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
