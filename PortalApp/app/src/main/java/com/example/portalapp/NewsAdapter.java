package com.example.portalapp;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/*
 * ListVew用のアダプタ
 */
public class NewsAdapter extends ArrayAdapter<ListInfo> {
	LayoutInflater inflater;

	public NewsAdapter(Context context, List<ListInfo> objects) {
		super(context, -1, objects);
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_latest_news, parent, false);
		}

		ListInfo head = getItem(position);
		ImageView imageView1 = (ImageView) convertView.findViewById(R.id.imageView_ico);
		TextView textView_msg = (TextView) convertView.findViewById(R.id.textView_msg);
		TextView textView_date = (TextView) convertView.findViewById(R.id.textView_date);
		TextView textView_title = (TextView) convertView.findViewById(R.id.textView_title);
		TextView textView_pdf = (TextView) convertView.findViewById(R.id.textView_pdf);

		textView_msg.setText(head.msg);
		textView_date.setText("更新日："+head.date);
		textView_title.setText("　カテゴリー："+head.title);
		textView_title.setTextColor(Color.rgb(120, 120, 120));
		textView_date.setTextColor(Color.rgb(120, 120, 210));
		
		if(head.link.endsWith(".pdf")){
			textView_pdf.setText("[PDF]　");
			textView_pdf.setTextColor(Color.rgb(255, 0, 0));
		}else{
			textView_pdf.setText("");
		}
		
		if(head.kategorie == 0){
			// 重要
			imageView1.setImageResource(R.drawable.important);
		}else if(head.kategorie == 1){
			// トピックス
			imageView1.setImageResource(R.drawable.topix);
		}else if(head.kategorie == 2){
			// お知らせ
			imageView1.setImageResource(R.drawable.message);
		}else if(head.kategorie == 3){
			// イベント
			imageView1.setImageResource(R.drawable.event);
		}else if(head.kategorie == 4){
			// 募集
			imageView1.setImageResource(R.drawable.recruit);
		}else{
			// その他
			imageView1.setImageResource(R.drawable.others);
		}
		
		return convertView;
	}
}
