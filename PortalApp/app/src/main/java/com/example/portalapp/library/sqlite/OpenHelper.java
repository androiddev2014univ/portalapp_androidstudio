package com.example.portalapp.library.sqlite;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import com.example.portalapp.library.sqlite.table.Post;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * 
 * @author teamMykata
 *
 */
public final class OpenHelper extends SQLiteOpenHelper {
	private String saltkey=null;
	
	@SuppressWarnings("unused")
	private static final String TAG = OpenHelper.class.getSimpleName();
	private static final String DB_NAME = "tmpinfo.db"; // DB名の設定
	private static final int DB_VERSION = 1;
	private static volatile OpenHelper sInstance;
	private static final String PREF_PARAM_ENCRYPT_KEY = "LK,<#@wWqoD*7R56=)]{!D?><D";
	private String mEncryptKey;
	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public synchronized static OpenHelper getInstance(Context context) {
	    if (sInstance == null) {
	        sInstance = new OpenHelper(context);
	    }
	    return sInstance;
	}
	/**
	 * 
	 * @param context
	 */
	public OpenHelper(Context context) {
		// TODO 自動生成されたコンストラクター・スタブ
		super(context, DB_NAME, null, DB_VERSION);
		SQLiteDatabase.loadLibs(context);
		this.generateEncryptKey(context);
		//this.generateLoginKey(context);
	}
	/**
	 * 
	 * @param saltkey
	 */
	public void setSaltKey(String saltkey){
		this.saltkey = saltkey;
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sqlcipher.database.SQLiteOpenHelper#onCreate(net.sqlcipher.database.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
	
	    Post.createTable(database);
	    //Post.insertValues("sample_user", "sample_passwd", database);
	    Post.insertValues("", "", database);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sqlcipher.database.SQLiteOpenHelper#onUpgrade(net.sqlcipher.database.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	    // TODO Auto-generated method stub
	
	}
	/**
	 * @param context
	 */
	private void generateEncryptKey(Context context) {
	    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	    this.mEncryptKey = sharedPreferences.getString(PREF_PARAM_ENCRYPT_KEY, null);
	    
	    if (TextUtils.isEmpty(this.mEncryptKey)) {
	        this.mEncryptKey = UUID.randomUUID().toString();
	        Editor editor = sharedPreferences.edit();
	        editor.putString(PREF_PARAM_ENCRYPT_KEY, this.mEncryptKey);
	        editor.commit();
	    }
	    
	}    
	/**
	 * 
	 * @return
	 */
	public synchronized SQLiteDatabase getReadableDatabase() {
	    return this.getReadableDatabase(encryptStr(this.mEncryptKey, ""+this.saltkey));
	}
	/**
	 * 
	 * @return
	 */
	public synchronized SQLiteDatabase getWritableDatabase() {
	    return this.getWritableDatabase(encryptStr(this.mEncryptKey, ""+this.saltkey));
	}
	/**
	 * 
	 * @param text
	 * @param salt
	 * @return
	 */
	private static String encryptStr(String text, String salt) {
	    MessageDigest md = null;
	    StringBuffer buf = new StringBuffer();
	 
	    try {
	        md = MessageDigest.getInstance("SHA-256");
	    } catch (NoSuchAlgorithmException e) {
	        Log.e("encryptStr-NoSuchAlgorithmException", "指定されたアルゴリズムが見つかりませんでした");
	    }
	 
	    md.update((text+salt).getBytes());
	    byte[] tmpAry = md.digest();
	    for(int i = 0; i < tmpAry.length; i++){
	        String tmpStr = Integer.toHexString(tmpAry[i] & 0xff);
	        if(tmpStr.length() == 1){
	            buf.append('0').append(tmpStr);
	        } else {
	            buf.append(tmpStr);
	        }
	    }
	    
	    return buf.toString();
	}
	
}
