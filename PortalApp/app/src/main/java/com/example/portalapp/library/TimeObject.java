package com.example.portalapp.library;

public class TimeObject{
	private long time;
	public TimeObject(long time){
		this.time = time / 1000;
	}
	public int gethour(){
		return (int)(this.time / 60 / 60 % 24);
	}
	public int getminuite(){
		return (int)(this.time / 60 % 60);
	}
	public int getsecond(){
		return (int)(this.time % 60);
	}
}
