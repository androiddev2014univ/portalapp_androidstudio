package com.example.portalapp.settings.devinfo;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.example.portalapp.R;
import com.example.portalapp.library.SP;
import com.example.portalapp.library.sqlite.ReadDB;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;


public class DevInfoActivity extends ActionBarActivity implements OnClickListener{
	private String SharedPreferenceFileName;
	private String isDevModeTagKey;
	private EditText text_mail;
	private Button btn_send;
	private ToggleButton tglbtn_devmode;
	private Handler reshandler, uihandler;
    private ProgressDialog progressDialog;
    private ReadDB loginInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devinfo);

    	progressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
    	progressDialog.setMessage("データ取得中...");
    	progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	progressDialog.setCancelable(false);
    	
    	progressDialog.show();
    	
        loginInfo = new ReadDB(getApplicationContext());
        SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_devinfo);
        isDevModeTagKey = getString(R.string.shared_preferences_devinfo_tag_name_isdevmode);
        
        btn_send = (Button) findViewById(R.id.button_post);
        btn_send.setOnClickListener(this);
        text_mail = (EditText) findViewById(R.id.text_mail);
        tglbtn_devmode = (ToggleButton) findViewById(R.id.toggle_dev_option);
        tglbtn_devmode.setOnClickListener(this);
        
    	reshandler = new Handler();
    	uihandler = new Handler();
    	
    	new Thread(){
    		String username;
    		@Override
    		public void run(){
    	    	try{
    	    		loginInfo.init();
    	    	}catch(Exception e){
    	    		e.printStackTrace();
    	    	}
    	    	username = loginInfo.getUsername();
    	    	if(username == null || username.equals("")){
    	    		username = "example";
    	    	}
    	    	uihandler.post(new Runnable(){
    	    		@Override
    	    		public void run(){
    	    	    	text_mail.setText(username+"@st.oit.ac.jp");
    	    			
    	    		}
    	    	});
		    	handlerPlog.sendEmptyMessage(0);
    		}
    	}.start();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	
    	SP sp = new SP(SharedPreferenceFileName, this);
    	boolean flg = sp.readBool(isDevModeTagKey);
    	tglbtn_devmode.setChecked(flg);
    	setEnableed(flg);
    	
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
    @SuppressLint("HandlerLeak") private Handler handlerPlog = new Handler() {
    	@Override
    	public void handleMessage(Message msg) {
    		// ロード終了
    		progressDialog.dismiss();
    	}
    };

	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		if(v.getId() == R.id.button_post){
			AlertDialog.Builder alert = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
			alert.setTitle("送信の確認");
			alert.setMessage("以下の宛先に送信します。\n"
					+"\""+text_mail.getText()+"\"");
			alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO 自動生成されたメソッド・スタブ
			    	progressDialog.setMessage("メール送信中...");
			    	progressDialog.show();
					// 送信
					post();
					
				}
			});
			alert.setNegativeButton("Cancel", null);
			alert.setCancelable(true);
			AlertDialog dialog = alert.create();
			dialog.show();
			
			
		}else if(v.getId() == R.id.toggle_dev_option){
			SP sp = new SP(SharedPreferenceFileName, this);
			if(tglbtn_devmode.isChecked()){
				sp.write(isDevModeTagKey, true);
		    	setEnableed(true);
				
			}else{
				sp.write(isDevModeTagKey, false);
		    	setEnableed(false);
				
			}
			
		}
		
	}
	
	private void post(){
		new Thread(){
			// この２変数はスクリプトを設置しているサーバによって値が異なります
			final String serverUrl = getString(R.string.post_email_server_url);
			final String scriptPath = getString(R.string.post_email_server_script_path);
			
			final String emailNameKey = getString(R.string.post_email_server_email_account_key);
			final String emailOrganizationKey = getString(R.string.post_email_server_email_organization_key);
			boolean isFinish = true;
			String response = null;
			ArrayList<Exception> errs = new ArrayList<Exception>();
			
			@Override
			public void run(){
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				String[] parts = text_mail.getText().toString().split("@");
				params.add(new BasicNameValuePair(emailNameKey , parts[0]));
				params.add(new BasicNameValuePair(emailOrganizationKey , "@" + parts[1]));
				DefaultHttpClient client = new DefaultHttpClient();
				HttpResponse httpResponse = null;
				
				String query = URLEncodedUtils.format(params, "UTF-8");
				HttpGet httpGet = new HttpGet(serverUrl + scriptPath + query);
				try {
					httpResponse = client.execute(httpGet);
				} catch (ClientProtocolException e1) {
					// TODO 自動生成された catch ブロック
					errs.add(e1); isFinish = false;
				} catch (IOException e1) {
					// TODO 自動生成された catch ブロック
					errs.add(e1); isFinish = false;
				} catch (Exception e) {
					errs.add(e); isFinish = false;
				}
				
				if (httpResponse != null){
					// int statusCode = httpResponse.getStatusLine().getStatusCode();
					
					HttpEntity entity = httpResponse.getEntity();
					try {
						response = EntityUtils.toString(entity);
						entity.consumeContent();
						
					} catch (ParseException e) {
						// TODO 自動生成された catch ブロック
						errs.add(e); isFinish = false;
					} catch (IOException e) {
						// TODO 自動生成された catch ブロック
						errs.add(e); isFinish = false;
					} catch (Exception e) {
						errs.add(e); isFinish = false;
					}
					
				}
				
				client.getConnectionManager().shutdown();
				
				reshandler.post(new Runnable(){
					@Override
					public void run(){
						if(response == null){
							isFinish = false;
						}else if(response.endsWith("false")){
							isFinish = false;
						}
						
						if (isFinish){
							Toast.makeText(DevInfoActivity.this, "メールを送信しました", Toast.LENGTH_SHORT).show();
						
						}else{
							Toast.makeText(DevInfoActivity.this, "メール送信に失敗しました", Toast.LENGTH_SHORT).show();
							/*for(int i=0;i<errs.size();i++){
								Toast.makeText(MainActivity.this, errs.get(i).toString(), Toast.LENGTH_SHORT).show();
								
							}*/
							
						}
						
					}
				});
				
		    	handlerPlog.sendEmptyMessage(0);
			}
		}.start();
	}
	
	private void setEnableed(boolean flg){
		btn_send.setEnabled(flg);
		text_mail.setEnabled(flg);
		text_mail.setFocusable(flg);
		text_mail.setFocusableInTouchMode(flg);
		if(flg){
			text_mail.requestFocus();
			text_mail.setSelection(0);
			text_mail.setTextColor(Color.rgb(221, 221, 221));
			
		}else{
			text_mail.setTextColor(Color.rgb(90, 90, 90));
			
		}
	}
	
}

