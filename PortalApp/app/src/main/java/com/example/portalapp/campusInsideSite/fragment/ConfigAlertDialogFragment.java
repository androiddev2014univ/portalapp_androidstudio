package com.example.portalapp.campusInsideSite.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.example.portalapp.R;
import com.example.portalapp.library.SP;

public class ConfigAlertDialogFragment extends DialogFragment {
	private Context context;
	private String SharedPreferenceFileName_LoadPage;
	private String LoadPageNumTagKey;
	private SP sp;
	
	public ConfigAlertDialogFragment(Context context){
		this.context = context;
		SharedPreferenceFileName_LoadPage = this.context.getString(R.string.shared_preferences_file_name_loadPage);
		LoadPageNumTagKey = this.context.getString(R.string.shared_preferences_loadPage_tag_name_loadPageSignal);
		sp = new SP(SharedPreferenceFileName_LoadPage, this.context);
		
	}
	
    private int singleChoicePosition = 0;
    @Override
	public Dialog onCreateDialog(Bundle savedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
		CharSequence[] array = getResources().getStringArray(R.array.portal_load_page_num_names);
		singleChoicePosition = 0;
		
		builder.setTitle("最初に読み込む件数を選択")
		.setSingleChoiceItems(array, sp.readInt(LoadPageNumTagKey), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				singleChoicePosition = which;
			}
		});
		builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				/*Toast.makeText(getActivity(),
						"OK Clicked! singleChoicePosition:"+singleChoicePosition, Toast.LENGTH_SHORT
						).show();*/
				
				sp.write(LoadPageNumTagKey, singleChoicePosition);
				
			}
		});
		builder.setNegativeButton("Cancel", null);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		
		return dialog;
	}
 
    @Override
    public void onStop() {
        super.onStop();
        // Log.v("onStopLog", "Stopped");
        
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Log.v("onDestroy", "Destroyed");
        
    }
    
}