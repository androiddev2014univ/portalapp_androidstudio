package com.example.portalapp.bustime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.example.portalapp.R;
import com.example.portalapp.bustime.BusRoute.Time;
import com.example.portalapp.library.SP;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BusTimeActivity extends ActionBarActivity implements
        OnItemSelectedListener {

	private String SharedPreferenceFileName;
	private String typeSignalTagKey;
	private String KEIHANBUS_SCRIPT_URL;
	
    private TextView resultText;
    private Spinner directionSpinner;
    private String[] urls;
    private ArrayList<ArrayList<Integer>> routesIndexes;
    private ActionBarActivity activity;
    ProgressDialog progressDialog;

    private TreeMap<String, String> busResult;
    private final Handler handler = new Handler();

    /**
     * 
     * @param index
     * @param isShowProgressDialog
     */
    private void loadBusData(final int index, final boolean isShowProgressDialog) {
    	try {
	    	// プログレスダイアログを有効にする
	    	if(isShowProgressDialog) progressDialog.show();
    	} catch (Exception e) {
    		e.printStackTrace();
    		
    	}

        new Thread(new Runnable() {
            BusTimeTable table;
            
            @Override
            public void run() {
                
                try {
                    Document doc = Jsoup.connect(urls[index]).get();
                    Element htmlTable = doc.getElementById("weekday");
                    KeihanBusTimeTableParser parser = new KeihanBusTimeTableParser();
                    table = parser.parse(htmlTable);

                } catch (final Exception e) {
                	handler.post(new Runnable(){
                		@Override
                		public void run(){
	                        Toast.makeText(activity,
	                                "バスのデータの所得に失敗しました...\n" + e.toString(),
	                                Toast.LENGTH_SHORT).show();
	                        
                		}
                		
                	});

                }

                runOnUiThread(new Runnable() {
                	@Override
                    public void run() {
                        try {
                            laodBusData(table, index);
                            showBusData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                	
                });

                try {
                	// プログレスダイアログを無効にする
                	if(isShowProgressDialog) handlerPlog.sendEmptyMessage(0);
                } catch(Exception e) {
                	e.printStackTrace();
                	
                }
            }

        }).start();
    }

    private void showBusData() {

        String result = "";
        int cnt = 0;
        for (Map.Entry<String, String> e : busResult.entrySet()) {
            result += e.getKey() + "," + e.getValue() + "\n";
            cnt++;

            if (cnt == 5) {
                break;
            }
        }

        resultText.setText(result);
    }

    /**
     * 
     * @param table
     * @param index
     */
    private void laodBusData(BusTimeTable table, int index) {
        Calendar date = Calendar.getInstance();

        int nowHour = date.get(Calendar.HOUR_OF_DAY);
        int nowMinute = date.get(Calendar.MINUTE);

        for (int h = nowHour; h <= nowHour + 1; h++) {
            for (Integer routeIndex : routesIndexes.get(index)) {

                BusRoute busRoute = table.getRoute(routeIndex.intValue());
                Time time = busRoute.getTime(h);
                Time.Minute[] minutes = time.minutes;

                if (minutes == null) {
                    continue;
                }

                for (int i = 0; i < minutes.length; i++) {
                    if (h == nowHour && minutes[i].intValue() < nowMinute) {
                        continue;
                    }

                    busResult.put(String.format("%02d:%02d", h,
                            minutes[i].intValue()), busRoute.getDest()
                            .replaceAll("¥n", " "));
                }

            }
        }

    }
    
    // _____________________________________________________________________________________________________
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_time);
        
        activity = this;
        SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_bustime);
        typeSignalTagKey = getString(R.string.shared_preferences_bustime_tag_name_typeSignal);
        KEIHANBUS_SCRIPT_URL = getString(R.string.keihanbus_script_url);

        resultText = (TextView) this.findViewById(R.id.result_text);
        directionSpinner = (Spinner) this.findViewById(R.id.direction_spinner);
        directionSpinner.setOnItemSelectedListener(this);
        
        SP sp = new SP(SharedPreferenceFileName, this);
        directionSpinner.setSelection(sp.readInt(typeSignalTagKey));

    	progressDialog = new ProgressDialog(this);
    	progressDialog.setMessage("Now Loading...");
    	progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	progressDialog.setCancelable(false);

        /*
         * 上から
         * 長尾 -> 情報科学部
         * 情報科学部 -> 長尾
         * 樟葉 -> 情報科学部
         * 情報科学部 -> 樟葉
         * 情報科学部 -> 樟葉
         * 
         */
        urls = new String[5];
        urls = getResources().getStringArray(R.array.query_lists);
        for(int i=0;i<5;i++){
        	urls[i] = KEIHANBUS_SCRIPT_URL + urls[i];
        }

        routesIndexes = new ArrayList<ArrayList<Integer>>();
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(0, 1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));

        busResult = new TreeMap<String, String>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }

    /*
     * アイテムが選択された時
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {

        resultText.setText("loading data");
        busResult.clear();

        loadBusData(position, true);
        if (position == 3) {
            loadBusData(4, true);
        }
        showBusData();
        
        SP sp = new SP(SharedPreferenceFileName, this);
        sp.write(typeSignalTagKey, position);

    }

    /*
     * アイテムが選択されなかったとき
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO 自動生成されたメソッド・スタブ

    }
    
    /*
     * プログレスダイアログを無効にするハンドラ
     */
    @SuppressLint("HandlerLeak") private Handler handlerPlog = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	// ロード終了
        	progressDialog.dismiss();
        }
    };

}
