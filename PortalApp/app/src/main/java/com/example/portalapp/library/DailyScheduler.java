package com.example.portalapp.library;

import java.util.Calendar;
import java.util.TimeZone;
 
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
 
/*
 * 参考URL：http://kimihiro-n.appspot.com/show/343001
 */
public class DailyScheduler {
 
	private Context context;
	
	public DailyScheduler(Context context) {
		this.context = context;
	}
 
	/**
	 * duration_time(ミリ秒)後 launch_serviceを実行する
	 * service_idはどのサービスかを区別する為のID(同じなら上書き)
	 * 1回起動するとそのタイミングでinterval_time毎に動き続ける
	 * @param launch_service
	 * @param duration_time
	 * @param service_id
	 * @param interval_time
	 */
	public <T> void set(Class<T> launch_service, long duration_time, int service_id, long interval_time) {
		
		Intent intent = new Intent(this.context, launch_service);
		
		PendingIntent action = PendingIntent.getService(this.context, service_id, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarm = (AlarmManager) this.context
				.getSystemService(this.context.ALARM_SERVICE);
		alarm.setRepeating(AlarmManager.RTC,
				duration_time, interval_time, action);
	}
 
	/**
	 * 起動したい時刻(hour:minuite:second)を指定するバージョン
	 * 指定した時刻で起動する
	 * @param launch_service
	 * @param hour
	 * @param minuite
	 * @param second
	 * @param service_id
	 * @param interval_time
	 */
	public <T> void setByTime(Class<T> launch_service, int hour, int minuite, int second , int service_id, long interval_time) {
		// 日本(+9)以外のタイムゾーンを使う時はここを変える
		TimeZone tz = TimeZone.getTimeZone("Asia/Tokyo");
		
		//今日の目標時刻のカレンダーインスタンス作成
		Calendar cal_target = Calendar.getInstance();
		cal_target.setTimeZone(tz);		
		cal_target.set(Calendar.HOUR_OF_DAY, hour);
		cal_target.set(Calendar.MINUTE, minuite);
		cal_target.set(Calendar.SECOND, second);
 
		//現在時刻のカレンダーインスタンス作成
		Calendar cal_now = Calendar.getInstance();
		cal_now.setTimeZone(tz);
		
		//ミリ秒取得
		long target_ms = cal_target.getTimeInMillis();
		long now_ms = cal_now.getTimeInMillis();
 
		// 24時間で割り切れれば、その日分遅らせて時間ををセットする
		if(interval_time % AlarmManager.INTERVAL_DAY == 0){
			int delta_day = (int)(interval_time / AlarmManager.INTERVAL_DAY);
			//Toast.makeText(this.context, delta_day+" Days Later.", Toast.LENGTH_SHORT).show();
			cal_target.add(Calendar.DAY_OF_MONTH, delta_day);
			target_ms = cal_target.getTimeInMillis();
			set(launch_service, target_ms, service_id, interval_time);
			
		// 割り切れなかった場合は、その時間をセットする
		}else{
			//今日ならそのまま指定
			if (target_ms >= now_ms) {
				set(launch_service, target_ms, service_id, interval_time);
			//過ぎていたら明日の同時刻を指定
			} else {
				cal_target.add(Calendar.DAY_OF_MONTH, 1);
				target_ms = cal_target.getTimeInMillis();
				set(launch_service, target_ms, service_id, interval_time);
			}
			
		}
 
	}
 
	/**
	 * キャンセル用
	 * @param launch_service
	 * @param wake_time 特に関係ない
	 * @param service_id
	 */
	public <T> void cancel(Class<T> launch_service, long wake_time, int service_id) {
		Intent intent = new Intent(this.context, launch_service);
		PendingIntent action = PendingIntent.getService(this.context, service_id, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager) this.context
				.getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(action);
	}
}