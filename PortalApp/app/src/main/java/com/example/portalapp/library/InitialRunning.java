package com.example.portalapp.library;

import com.example.portalapp.R;
import com.example.portalapp.library.SP;

import android.content.Context;
import android.widget.Toast;

/*
 * 初回のみ実行される処理を記述
 */
public class InitialRunning {
	private String SharedPreferenceFileName;
	private String isInitKeyName;
	private Context context;
	
	public InitialRunning(Context context){
		this.context = context;
		SharedPreferenceFileName = this.context.getString(R.string.shared_preferences_file_name_InitState);
		isInitKeyName = this.context.getString(R.string.shared_preferences_InitState_tag_name_isInit);
		
	}
	
	public void InitialRun(){
		try {
			if(this.isState()){
				//-------------------------------------------------------------------------------------------
				// 初回時にのみ実行する処理
				String SharedPreferenceFileName = this.context.getString(R.string.shared_preferences_file_name_notifi);
				String isNotifiTagKey = this.context.getString(R.string.shared_preferences_notifi_tag_name_isnotifi);
				String intervalTagKey = this.context.getString(R.string.shared_preferences_notifi_tag_name_interval_sig);
				String isServiceNotificationKey = this.context.getString(R.string.shared_preferences_notifi_tag_name_isServiceNotification);
				SP sp = new SP(SharedPreferenceFileName, this.context);
				sp.write(isNotifiTagKey, false); // 新着通知を受け取らない
				sp.write(intervalTagKey, 6); // サービスを１日毎に起動する
				sp.write(isServiceNotificationKey, false); // サービスが実行されていることを伝える通知バーを受け取らない
				//-------------------------------------------------------------------------------------------
				Toast.makeText(context, "PortallAppの初期化処理を完了しました", Toast.LENGTH_LONG).show();
				this.setState();
			}
		} catch (Exception e) {
				Toast.makeText(context, "PortallAppの初期化処理でエラーが発生しました："+e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	
	private boolean isState(){
		SP_Alpha sp = new SP_Alpha(SharedPreferenceFileName, context);
		return sp.readBool(isInitKeyName);
		
	}
	
	private void setState(){
		SP_Alpha sp = new SP_Alpha(SharedPreferenceFileName, context);
		sp.write(isInitKeyName, false);
		
	}
	
	private class SP_Alpha extends SP{
		public SP_Alpha(String sp_name, Context context){
			super(sp_name, context);
		}
		@Override
		public boolean readBool(String key){
			if(super.sp != null) return super.sp.getBoolean(key, true);
			else return true;
		}
		
	}
	
	
	
}
