package com.example.portalapp.campusInsideSite.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.adapter.SiteNewsListAdapter;

public class UnivPortalListFragment extends Fragment {

    public ListView mListView;
    private SiteNewsListAdapter mAdapter;
    private Activity parentActivity;
    private OnItemClickListener mItemClickListener;
    
    private static UnivPortalListFragment univPortalListFragment;
    public static UnivPortalListFragment getInstance() {
        if (univPortalListFragment == null) {
            univPortalListFragment = new UnivPortalListFragment();
        }
        return univPortalListFragment;
    }
    
    private UnivPortalListFragment() {
        
    }

    @SuppressLint("InflateParams") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        return inflater.inflate(R.layout.univ_portal_list_fragment, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        parentActivity = getActivity();
        mListView = (ListView) parentActivity.findViewById(R.id.univ_portal_list);
        mListView.setOnItemClickListener(mItemClickListener);

    }
    
    public void setAdapter(SiteNewsListAdapter adapter) {
        mAdapter = adapter;
        mListView.setAdapter(mAdapter);
    }
    
    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
        
    }

}
