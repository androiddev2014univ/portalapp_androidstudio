package com.example.portalapp.campusInsideSite.listInfo;

public class ListLocation {
	public int message_position;
	public int univ_position;
	public int message_distance;
	public int univ_distance;
	
	public ListLocation(){
		clearLocation();
		
	}
	public ListLocation(
			int message_position,
			int univ_position,
			int message_distance,
			int univ_distance
	) {
		setLocation(message_position, univ_position, message_distance, univ_distance);
		
	}
	
	public void setLocation(
			int message_position,
			int univ_position,
			int message_distance,
			int univ_distance
	) {
		this.message_position = message_position;
		this.univ_position = univ_position;
		this.message_distance = message_distance;
		this.univ_distance = univ_distance;
		
	}
	public void clearLocation() {
		setLocation(0, 0, 0, 0);
		
	}

}
