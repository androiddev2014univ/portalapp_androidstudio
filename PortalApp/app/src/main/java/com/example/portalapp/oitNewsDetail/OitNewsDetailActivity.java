package com.example.portalapp.oitNewsDetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.portalapp.R;

public class OitNewsDetailActivity extends ActionBarActivity{
	WebView oit_web;
	private String url;
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oitnewsdetail);
        
        oit_web = (WebView) findViewById(R.id.oitLatestNews);
        oit_web.setWebViewClient(new WebViewClient());
        
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        
        oit_web.loadUrl(url);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }

}
