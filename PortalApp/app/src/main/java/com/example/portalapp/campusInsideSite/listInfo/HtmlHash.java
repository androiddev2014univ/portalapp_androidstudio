package com.example.portalapp.campusInsideSite.listInfo;

public class HtmlHash {
	public String title =null;
	public String date =null;
	public String org =null;
	public String type =null;
	protected String url =null;
	
	public HtmlHash() {
		
	}
	public HtmlHash(String title, String date, String org, String type, String url) {
		this.set(title, date, org, type, url);
		
	}
	public void set(String title, String date, String org, String type, String url) {
		this.title = title;
		this.date = date;
		this.org = org;
		this.type = type;
		this.url = url;
	}
	
}
