package com.example.portalapp.library;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/*
 * SharedPrefrencesにアクセスするクラス
 */
public class SP {
	protected SharedPreferences sp;
	public SP(String sp_name, Context context){
		sp = context.getSharedPreferences(sp_name, Context.MODE_PRIVATE);
	}
	public void write(String key, int num){
		Editor editor = sp.edit();
		editor.putInt(key, num);
		editor.commit();
	}
	public void write(String key, boolean flg){
		Editor editor = sp.edit();
		editor.putBoolean(key , flg);
		editor.commit();
	}
	public void write(String key, String value){
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}
	public int readInt(String key){
		if(sp != null) return sp.getInt(key, 0);
		else return 0;
	}
	public boolean readBool(String key){
		if(sp != null) return sp.getBoolean(key, false);
		else return false;
	}
	public String readStr(String key){
		if(sp != null) return sp.getString(key, "");
		else return "";
	}
}
