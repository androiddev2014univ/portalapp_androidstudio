package com.example.portalapp.about;

import com.example.portalapp.R;
import com.example.portalapp.lawRelation.LawRelationActivity;
import com.example.portalapp.library.Access;
import com.example.portalapp.license.LicenseActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends ActionBarActivity implements OnClickListener{
	TextView textCode, textName;
	Button btnDetail, btnUsingLib;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		Access access = new Access(this);
		textCode = (TextView) findViewById(R.id.textVersionCode);
		textName = (TextView) findViewById(R.id.textVersionName);
		btnDetail = (Button) findViewById(R.id.button_detail);
		btnDetail.setOnClickListener(this);
		btnUsingLib = (Button) findViewById(R.id.button_detail_using);
		btnUsingLib.setOnClickListener(this);
		
		textCode.setText("Version Code : "+access.getVersionCode());
		textName.setText("Version Name : "+access.getVersionName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }

	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		Intent intent = null;
		if (v.getId() == R.id.button_detail){
			intent = new Intent(this, LicenseActivity.class);
		}else if (v.getId() == R.id.button_detail_using){
			intent = new Intent(this, LawRelationActivity.class);
		}
		startActivity(intent);
	}
	
}
