package com.example.portalapp.campusInsideSite.extJsoup;

import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupLogin {
	final protected static String site_uri = "https://www.portal.oit.ac.jp/CAMJWEB/";

	final private static String top_script_name = "ktop.do";
	final private static String	login_script_name = "klogin.do";
	final private static String	news_page_script_name = "wbktmgjr.do";
	
	final private static String init_query = news_page_script_name + "?clearAccessData=true&contenam=wbktmgjr&kjnmnNo=5";
	final private static String	def_query = news_page_script_name + "?buttonName=navigateMessgList&pageCount=";
	
	final private static String content_type = "application/x-www-form-urlencoded;charset=UTF-8";
	final private static String user_id_tag_name = "userId";
	final private static String	passwd_tag_name = "password";
	final private static String	login_btn_tag_name = "login";
	final private static String login_btn_value_name = "ログイン";
	
	final private static int interval_connect_time_millisec = 6000;
	
	private String username = null, password = null;
	private Map<String, String> cookies;
	private Connection.Response newres;
	
	// コンストラクタ
	public JsoupLogin() {
		this.setInfo(null, null);
	}
	JsoupLogin(String name, String passwd, String uri) {
		this.setInfo(name, passwd);
		
	}
	
	/**
	 * 引数tmpのCookieをセットする
	 * @param tmp
	 */
	public void setCookies(Map<String, String> tmp){
		cookies = tmp;
	}
	/**
	 * Cookieを返却する
	 * @return
	 */
	public Map<String, String> getCookies(){
		return cookies;
	}
	/**
	 * ユーザ名とパスワードをセットする
	 * @param name
	 * @param passwd
	 */
	public void setInfo(String name, String passwd) {
		this.username = name;
		this.password = passwd;
		
	}
	/**
	 * ログインを実行する
	 * ログインに成功した場合、得られたCookieをセットし、初期接続処理を行う
	 * @throws Exception
	 */
	public void login() throws Exception {
		this.getCookiesProcess(top_script_name);
        String login_url= site_uri + login_script_name;
        Connection newreq = Jsoup
                .connect(login_url).cookies(this.cookies).followRedirects(true)
                .header("Connection", "keep-alive")
                .header("Refer", site_uri + top_script_name)
                .header("Content-Type", content_type)
                .data(user_id_tag_name,this.username).data(passwd_tag_name,this.password)
                .data(login_btn_tag_name, login_btn_value_name)
                .timeout(interval_connect_time_millisec).method(Method.POST);
        this.newres = newreq.execute();
        this.setCookiesProcess(this.newres);
        this.initConnect();
        
	}
	/**
	 * 引数page_numberで指定されたページのドキュメントを返却する
	 * @param page_number
	 * @return
	 * @throws Exception
	 */
	public Document getPortalPage(int page_number) throws Exception {
		page_number++;
		return Jsoup.connect(site_uri + def_query + page_number)
				.cookies(this.cookies).get();
	}
	/**
	 * 引数docのドキュメント内の指定された番号（accessed_index）のドキュメントを返却する
	 * @param doc
	 * @param accessed_index：0~9
	 * @return
	 * @throws Exception
	 */
	public Document getDetailPage(Document doc, int accessed_index) throws Exception {
		if(accessed_index < 0 || accessed_index > 9) {
			return null;
		}else{
	        Elements links = doc.getElementsByTag("a");
	        Element link = links.get(accessed_index);
	        String linkHref = link.attr("href");
	        return Jsoup.connect(site_uri + news_page_script_name + linkHref)
	        		.cookies(this.cookies).timeout(interval_connect_time_millisec).get();
	        
		}
        
	}
	// _________________________________________________________________________________________
	// クエリ送信をした後に、得られたCookieをセットする
	private void getCookiesProcess(String query) throws Exception {
		Connection.Response res = Jsoup
				.connect(site_uri + query).execute();
        this.setCookiesProcess(res);
        
	}
	// Cookieをセットする
	private void setCookiesProcess(Connection.Response res) {
		this.cookies = res.cookies();
		
	}
	// 初期接続をする
	private void initConnect() throws Exception {
		Jsoup.connect(site_uri + init_query)
		.cookies(this.cookies).get();
		
	}
	
}
