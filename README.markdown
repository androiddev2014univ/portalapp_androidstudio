PortalApp for Android Studio(higher Android4.2)
============
* #### [Smart Phone Application Design Project][link_android_app_project] at Osaka Institute of Technology
* #### プロジェクト文字コード：UTF-8
* #### 概要
	* 学内サイトにアクセスし大学のお知らせを受信します。また、京阪バスの時刻表を表示します。
	* 工大ニュース閲覧（ポータルサイト内の工大最新ニュース）
	* 学内のお知らせ閲覧（ログイン機能）
	* 通知機能（学内のお知らせを通知します）
	* 京阪バス時刻表ウィジェット（指定したバス停の時刻表のウィジェットです）

### ダウンロード
---
このアプリのAPKファイルは下記のリンクからダウンロードできます。

* Smart Phone Application Design Projectサイト内 [[Portalapp.apk][link_apk_file]]（要：学内VPN接続）

### 開発環境
---
* 必須
	* OS(Windows/Mac OS X)
	* Android Studio
	* Android SDK (*1)
	* Android NDK (*1)

* 推奨
	* Git（バージョン管理ツール）

(*1) JDK(Java SE Development Kit)がインストールされていて、環境変数（パス）が設定されていれば使用できます。JRE(Java Runtime Environment)ではありません。インストールされている場合は、端末（ターミナル）で`java -version`と`javac -version`と入力すると、ユーザ領域でのバージョンが確認できます。パスの確認は、Macの場合はホームディレクトリ内の設定ファイル内に`export JAVA_HOME=...`と記述されていればOKです。

### メモ
---
* Android Studio上で動作します(2015/3/22)
* メンバーでお問い合わせのアドレスを追記してもいい場合は、このマークダウンファイルに追記していってください(2015/2/3)
* ベータ版(ver1.0.1)をmasterにマージしました(2015/2/3)
* jni/フォルダ内のCPPファイルはアプリケーション（PortalApp）内の設定→開発者向けオプションのメール送信で取得できます（2015/01/12）
* ファイル（FILENAME.cpp）のロジックが漏れてしまうと、セキュリティ上の危険にさらされます（2014/12/13）

### ライブラリ
---
このアプリケーションで使用しているライブラリとそれらのライセンスは以下のとおりです（2015/2/2現在）

* SQLCipher for Android ver3.2.0（ https://www.zetetic.net/sqlcipher/ ）


			 Copyright (c) 2010 Zetetic LLC
	            All rights reserved.
	            
	            Redistribution and use in source and binary forms, with or without
	            modification, are permitted provided that the following conditions are met:
	                * Redistributions of source code must retain the above copyright
	                  notice, this list of conditions and the following disclaimer.
	                * Redistributions in binary form must reproduce the above copyright
	                  notice, this list of conditions and the following disclaimer in the
	                  documentation and/or other materials provided with the distribution.
	                * Neither the name of the ZETETIC LLC nor the
	                  names of its contributors may be used to endorse or promote products
	                  derived from this software without specific prior written permission.
	            
	            THIS SOFTWARE IS PROVIDED BY ZETETIC LLC ''AS IS'' AND ANY
	            EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	            DISCLAIMED. IN NO EVENT SHALL ZETETIC LLC BE LIABLE FOR ANY
	            DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	            (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	            LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	            ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	            (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	            SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

* Jsoup ver1.7.3（ http://jsoup.org ）

		jsoup License
		The jsoup code-base (include source and compiled packages) are distributed under
		the open source MIT license as described below.

		The MIT License
		Copyright © 2009 - 2014 Jonathan Hedley (jonathan@hedley.net)

		Permission is hereby granted, free of charge, to any person obtaining a copy of
		this software and associated documentation files (the "Software"), to deal in the
		Software without restriction, including without limitation the rights to use, copy,
		modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
		and to permit persons to whom the Software is furnished to do so, subject to the
		following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
		INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
		PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
		HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
		OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
		SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


### ライセンス
---
このアプリケーションは以下の[MITライセンス(wikipedia参照)][link_mit]で管理されます。


	The MIT License (MIT)

	Copyright (c) 2014 Team.Mykata

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in
	the Software without restriction, including without limitation the rights to
	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
	the Software, and to permit persons to whom the Software is furnished to do so,
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
	FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
	COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
	IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

	to Japanese

	以下に定める条件に従い、本ソフトウェアおよび関連文書のファイル（以下「ソフトウェア」）の複製を
	取得するすべての人に対し、ソフトウェアを無制限に扱うことを無償で許可します。
	これには、ソフトウェアの複製を使用、複写、変更、結合、掲載、頒布、サブライセンス、
	および/または販売する権利、およびソフトウェアを提供する相手に同じことを許可する権利も無制限に含まれます。

	上記の著作権表示および本許諾表示を、ソフトウェアのすべての複製または重要な部分に記載するものとします。

	ソフトウェアは「現状のまま」で、明示であるか暗黙であるかを問わず、何らの保証もなく提供されます。
	ここでいう保証とは、商品性、特定の目的への適合性、および権利非侵害についての保証も含みますが、
	それに限定されるものではありません。 作者または著作権者は、契約行為、不法行為、
	またはそれ以外であろうと、ソフトウェアに起因または関連し、あるいはソフトウェアの使用または
	その他の扱いによって生じる一切の請求、損害、その他の義務について何らの責任も負わないものとします。


### お問い合わせ
---
お問い合せは、下記メールアドレスにお気軽にどうぞ（[あっとまーく]は、@に置き換えてください）

* e1q12091[あっとまーく]st.oit.ac.jp


---
Copyright (c) 2014 Team.Mykata（チームまいかた）




[link_android_app_project]: http://lss.oit.ac.jp/~s-phone/main/
[link_mit]: http://ja.wikipedia.org/wiki/MIT_License
[link_apk_file]: http://lss.oit.ac.jp/~s-phone/main/apps/portal/Portalapp.apk

